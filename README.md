To use:

compile project.

then:

TypescriptToCSharpTranspiler.exe yourTypescriptDefinition.d.ts yourOutputFileName.cs

If you like this project, please make a little donation:

https://pledgie.com/campaigns/32542