﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Diagnostics;
using TypescriptToCSharpTranspiler.Reflection;
/*
 * 
 * 
 LAMBDAS SE TRANSCRIVENT EN Func<TResult, TP0, TP1, ...>

OBJETS CUSTOM SE TRANSCRIVENT EN CLASSES.



	Interfaces deviennent des classes.

	implements virent.

	extends restent.

	interface d'unions pour les multi types. IType1_OR_Type2 et on force ces types a implementer cette interface.

	donc la tout de suite: 
	gerer extends fait
	gerer implements fait
	gerer mot clé static. fait.
	gerer parametres optionnels. fait.
	gerer multi types. (machin: type0 | type1) //fait

	gerer parametres infinis. fonction(...args:any[])
	
	


	la documentation:


	/**
         * The geometry is created by sweeping and calculating vertexes around the Y axis (horizontal sweep) and the Z axis (vertical sweep). Thus, incomplete spheres (akin to 'sphere slices') can be created through the use of different values of phiStart, phiLength, thetaStart and thetaLength, in order to define the points in which we start (or end) calculating those vertices.
         *
         * @param radius — sphere radius. Default is 50.
         * @param widthSegments — number of horizontal segments. Minimum value is 3, and the default is 8.
         * @param heightSegments — number of vertical segments. Minimum value is 2, and the default is 6.
         * @param phiStart — specify horizontal starting angle. Default is 0.
         * @param phiLength — specify horizontal sweep angle size. Default is Math.PI * 2.
         * @param thetaStart — specify vertical starting angle. Default is 0.
         * @param thetaLength — specify vertical sweep angle size. Default is Math.PI.
         */
/*

		devient:
		///
		/// <summary>
		/// The geometry...
		/// </summary>
		/// <param name="radius"> sphere raidus ... </param>
		/// <returns></returns>

			cyrr

		 /**
         * Camera frustum right plane.
         */
//right: number;
/*

devient 
///
/// <sumary>
/// Camera frustrum right plane.
/// </sumary>
*/
/*

constructor(width: number, height: number, depth: number, widthSegments?: number, heightSegments?: number, depthSegments?: number);
*/

/*
export function warn(message?: any, ...optionalParams: any[]): void; // A FAIRE!

static create(root: any, path: any, parsedPath?: any): PropertyBinding|PropertyBinding.Composite;

le mot clé static est pas géré, ni les multi types.

a la limite les commentaires de documentation. mais bon vraiment pour etre pointitilleu.


attention ya des export const qui sont du type d'un enum, qui sont en fait les valeurs de l'enum.

commentaires. OK

exportKW a gerer => a la transcryption

variables directement dans le namespace => transforme le namespace en classe statique. 
// non du coup c'est a la transcryption qu'on se rends compte qu'un nom est direct dans un namespace ou meme en dehors de tout.



types generiques:
Array<Function[]> => Array <(in) Function [] >(out) // ca oui tout de suite. µ// c bon.

voila hardcorre salace a gerer:
test: {[index: string] : number}  // ca aussi (lire) // test: { listener: ((event: Event) => void)[], type: string, anotherObj: {} };

test: { listener: ((event: Event) => {}[])[], type: string, anotherObj:{} };


// debut de parenthese après colons: type entre parentheses et pas arguments de fonction.

// un array peut suivre a un type entre parentheses ou a un type tout court.

// sauf si => après dans ce cas la c'est bien les parametres d'une fonction lambda,

// ce qui suit => est un type de retour.

// on est en train de parler d'un type et on trouve {  c'est que nous avons une description d'objet.




parametres optionnels func(parsedPath?: any) // 

class smthg extends anotherclass // ca aussi

multi types: addAttribute(name: string, attribute: BufferAttribute|InterleavedBufferAttribute): BufferGeometry; // ca aussi (lire)

correspond a: (et les deux classes implémentes)

interface IBufferAttribute_U_InterleavedBufferAttribute
{
}


implements someClassOrInterface

ca ca fait mal:
groups: {start: number, materialIndex: number}[]; // ca aussi (lire) // jusque la aujourd'hui.

correspondra a:

interface IStart_MaterialIndex
{
start: number;
materialIndex: number;
}

groups: IStart_MaterialIndex[] 




et dicos aussi.

lambdas:
addEventListener(type: string, listener: (event: Event) => void ): void;  // ca aussi


wtf???
dispatchEvent(event: { type: string; [attachment: string]: any; }): void; // ca aussi.

a priori c'est un objet qui implémente:

au pire faire un dictionnaire de ce genre de truc, si on retrouve une interface avec les memes trucs dedans, on utilise la meme.


interface IType_Attachment {
type: string;
[attachment: string]: any;
}





 */
using System.Globalization;



public static class Extensions
{
	public static List<T> Clone<T>(this List<T> toClone)
	{
		List<T> res = new List<T>();
		for (int i = 0; i < toClone.Count; i++)
		{
			res.Add(toClone[i]);
		}
		return (res);
	}
	public static string CapitalizeFirstLetter(this String input)
	{
		if (string.IsNullOrEmpty(input))
			return input;

		return input.Substring(0, 1).ToUpper() +
			input.Substring(1, input.Length - 1);
	}
	public static string Capitalize(this string input)
	{
		StringBuilder sb = new StringBuilder(input);
		if (sb[0] >= 'a' && sb[0] <= 'z')
		{
			sb[0] = ((char)((int)sb[0] - (int)32));
		}
		//sb[0] = (sb[0] >= 'a' && sb[0] <= 'z') ? ((char)((int)sb[0] - (int)32));
		return (sb.ToString());
	}
}
public enum FuncType
{
	Func,
	TypeExpression,
	LambdaFunction
}
public enum ScopeType
{
	Scope,
	ObjectDescriptor
}
public class LineInfo
{
	public int lineNb = 0;
	public int minTabs = 0;
}
namespace TypescriptToCSharpTranspiler
{
	public enum IndexerType
	{
		Number,
		String
	}
	#region CSharpVersion
	public class ISmthg2
	{
		public Func<string> test;
	}
	public class ISmthg1
	{
		public Func<ISmthg2> test;
		public string[][] test2;
	}
	public class ISmthg3
	{
		public string this[string index]
		{
			get
			{
				return (((dynamic)this)[index]);
			}
			set
			{
				((dynamic)this)[index] = value;
			}
		}
	}
	public class EnglobigClass
	{

		public Func<ISmthg1, ISmthg3> test;
	}
	#endregion
	/*	
			typescript version:
		 	export class EnglobingClass
			{
				test: (param: { test: (() => {test: () => string}), test2: Array<((string)[])>}) => { [index: string]: string};
			}

			description actions effectuées:
				export class EnglobingClass
				{

				===> on va faire une class englobante.

				test:  une fonction lambda, son nom sera test
				===> public Func< 
				test: (param: { // le premier parametre de la fonction lambda est un descripteur d'objet, nous allons demander son nom de type virtuel, il nous retourne ISmthg1,
				
				===> public Func<ISmthg1;

				Et! le tout en crééant la classe:
				public class ISmthg1
				{
				celle ci continue alors de se construire...
				(param:{ test: (() = > {test: () => string}),
				cet objet contient un membre test, celui ci est un lambda qui retourne un autre objet<ISmthg2> (qui ira se creer dans la foulée),  qui contient a son tour un lambda qui renvoit un string.

					public Func<ISmthg2>
				

		////
				la classe ISmthg2 se créee a son tour pour l'objet.
			public class ISmthg2
			{
				public Func<string> test;
			}
		/////

			mais ce n'est pas le dernier membre de l'objet,
			il y a test2: Array<((string)[])>
			qui va se traduire simplement d'abord Array<string[]> car les parentheses sont inutiles a la compréhension, ensuite Array sera changé en[] suplémentaire au contenu de son parametre generique.
			nous donne donc:

			public string[][] test2 ;
			
				
			
	*/



	public class LanguageToken
	{
		public bool isName = false;
		public string formatedVarName
		{
			get
			{
				return (BuiltinTypes.ContainsValue(this.tokenString) ? "@" + this.tokenString : this.tokenString);
			}
		}
		public static bool readyToBuild = false;
		public List<LanguageToken> parameters = null;
		public LanguageToken LastLogicWithDeletedKeywords
		{
			get
			{
				LanguageToken current = this.last;
				while (current != null && (current.tokenType == "declareKW" || current.tokenType == "exportKW" || current.tokenType == "constKW" || current.tokenType == "publicKW" || current.tokenType == "space_spacement" || current.tokenType == "tab_spacement" || current.tokenType == "staticKW" || (readyToBuild && current.tokenType == "funcIN" && current.funcType == FuncType.TypeExpression)))
				{
					current = current.last;
				}
				if (current == this)
					return (null);
				return (current);
			}
		}
		public LanguageToken LastLogic
		{
			get
			{
				LanguageToken current = this.last;
				while (current != null && (current.tokenType == "space_spacement" || current.tokenType == "tab_spacement" || (readyToBuild && current.tokenType == "funcIN" && current.funcType == FuncType.TypeExpression)))
				{
					current = current.last;
				}
				if (current == this)
					return (null);
				return (current);
			}
		}
		public LanguageToken GetLastLogic(out string trimms, bool trimNewLines = false)
		{
			trimms = "";
			LanguageToken current = this.last;
			while (current != null && (current.tokenType == "space_spacement" || current.tokenType == "tab_spacement" || (current.tokenType == "newLine" && trimNewLines) || (readyToBuild && current.tokenType == "funcIN" && current.funcType == FuncType.TypeExpression)))
			{
				trimms += current.tokenString;
				current = current.last;
			}
			if (current == this)
			{
				return (null);
			}
			return (current);
		}
		public LanguageToken GetNextLogic(bool trimNewLines = false, StringBuilder sb = null, bool appendTrim = false)
		{
			if (sb == null)
			{
				string str = "";
				return (GetNextLogic(out str, trimNewLines));
			}
			else
			{
				string str = "";
				LanguageToken res = (GetNextLogic(out str, trimNewLines));
				if (appendTrim)
					sb.Append(str);
				return (res);
			}
		}
		public LanguageToken GetNextLogic(out string trimms, bool trimNewLines = false)
		{
			trimms = "";
			LanguageToken current = this.next;
			while (current != null && (current.tokenType == "space_spacement" || current.tokenType == "tab_spacement" || (current.tokenType == "newLine" && trimNewLines) || (readyToBuild && current.tokenType == "funcIN" && current.funcType == FuncType.TypeExpression)))
			{
				trimms += current.tokenString;
				current = current.next;
			}
			if (current == this)
			{
				return (null);
			}
			return (current);
		}

		public LanguageToken NextLogicSkipNewLines
		{
			get
			{
				LanguageToken current = this.next;
				while (current != null && (current.tokenType == "space_spacement" || current.tokenType == "tab_spacement" || current.tokenType == "newLine" || (readyToBuild && current.tokenType == "funcIN" && current.funcType == FuncType.TypeExpression)))
				{
					current = current.next;
				}
				if (current == this)
				{
					return (null);
				}
				return (current);
			}
		}
		public LanguageToken NextLogic
		{
			get
			{
				LanguageToken current = this.next;
				while (current != null && (current.tokenType == "space_spacement" || current.tokenType == "tab_spacement" || (readyToBuild && current.tokenType == "funcIN" && current.funcType == FuncType.TypeExpression)))
				{
					current = current.next;
				}
				if (current == this)
				{
					return (null);
				}
				return (current);
			}
		}
		public LanguageToken targetType = null;
		public LanguageToken typeTarget = null;
		public FuncType funcType = FuncType.Func;
		public ScopeType scopeType = ScopeType.Scope;

		public string exportedTypeName = "";
		//public LanguageToken nextAfterExportedType = null;
		//public static Dictionary<int, LanguageToken> exportedTypes = new Dictionary<int, LanguageToken>();
		public static Dictionary<string, string> translations = new Dictionary<string, string>() { {"equal", "="}, {"infiniteArgs","..." }, {"or","|" }, {"optionnalParameter","?" }, {"newLine", "\n" }, {"funcIN", "(" }, {"funcOUT", ")" }, {"scopeIN", "{" },
																								{"scopeOUT",  "}"}, {"semicolon",  ";"}, {"colons", ":"}, {"comma",  ","}, {"dot",  "."}, {"typeIN",  "<"}, {"typeOUT",  ">"}, {"arrayIN",  "["}, {"arrayOUT",  "]"}, {"lambdaArrow", "=>"},
		  {"enumKW","enum" }, {"constKW","const" }, {"staticKW", "static" }, {"publicKW", "public" }, {"privateKW", "private" }, {"classKW", "class" }, {"exportKW", "export" }, {"declareKW", "declare" }, {"interfaceKW", "interface" }, {"namespaceKW", "namespace" }, {"thisKW", "this" }, {"constructorKW", "constructor" }, {"implementsKW","implements" }, {"extendsKW","extends" }, {"varKW", "var" }, {"moduleKW", "module" } };

		public static Dictionary<string, string> scopeBeginers = new Dictionary<string, string>() { { "(", "funcIN" }, { "{", "scopeIN" }, { "<", "typeIN" }, { "[", "arrayIN" } };
		public static Dictionary<string, string> scopeEnders = new Dictionary<string, string>() { { ")", "funcOUT" }, { "}", "scopeOUT" }, { ">", "typeOUT" }, { "]", "arrayOUT" } };
		public static Dictionary<string, string> scopeDelimiters = new Dictionary<string, string>() { { "<", "typeIN" }, { ">", "typeOUT" }, { "[", "arrayIN" }, { "]", "arrayOUT" } , { "(", "funcIN" }, { ")", "funcOUT" }, { "{", "scopeIN" },
																								{ "}", "scopeOUT" } };
		// type => can be typed.
		public static Dictionary<string, bool> nameKinds = new Dictionary<string, bool>() { { "objectDescriptorMemberName", true }, { "className", false }, { "functionName", true }, { "methodName", true }, { "varName", true }, { "memberName", true }, { "parameterName", true }, { "builtinValue", false }, { "namespaceVar", true }, { "constantVar", true }, { "indexerName", true } };

		public static Dictionary<string, string> tokenEnders = new Dictionary<string, string>() { {"...", "infiniteArgs" }, {"|", "or" }, {"=", "equal" }, {"?", "optionnalParameter" }, { "\n", "newLine" }, { "(", "funcIN" }, { ")", "funcOUT" }, { "{", "scopeIN" },
																								{ "}", "scopeOUT" }, { ";", "semicolon" }, {":", "colons" }, { ",", "comma" }, { ".", "dot" }, { "<", "typeIN" }, { ">", "typeOUT" }, { "[", "arrayIN" }, { "]", "arrayOUT" }, {"=>", "lambdaArrow" }, {"\'", "textLittle" }, {"\"", "textBig" }, {"-", "minus" }, {"+", "plus" }, { "/", "div" }, {"*", "mult" } };

		public static Dictionary<string, string> nameEnders = new Dictionary<string, string>() { { ",", "comma" }, { ";", "semicolon" } };

		public static Dictionary<string, string> scopeDescriptors = new Dictionary<string, string>() { { "class", "classKW" }, { "interface", "interfaceKW" }, { "namespace", "namespaceKW" }, { "enum", "enumKW" }, { "module", "moduleKW" } };
		public static Dictionary<string, string> structDescriptors = new Dictionary<string, string>() { { "class", "classKW" }, { "interface", "interfaceKW" } };
		//keywordTokens.FirstOrDefault(x => x.Value == "one").Key;
		public static Dictionary<string, string> keywordTokens = new Dictionary<string, string>() { { "def", "defKW" }, { "import", "importKW" }, { "as", "asKW" }, { "enum", "enumKW" }, { "const", "constKW" }, { "static", "staticKW" }, { "public", "publicKW" }, { "private", "privateKW" }, { "class", "classKW" }, { "export", "exportKW" }, { "declare", "declareKW" }, { "interface", "interfaceKW" }, { "namespace", "namespaceKW" }, { "var", "varKW" }, { "this", "thisKW" }, { "constructor", "constructorKW" }, { "implements", "implementsKW" }, { "extends", "extendsKW" }, { "function", "functionKW" }, { "let", "letKW" }, { "module", "moduleKW" } };
		public static Dictionary<string, string> pythonEvaluationOperators = new Dictionary<string, string>() {
			//ARITHMETIC
			{ "+", "__Add" }, { "-", "__Sub" }, { "*", "__Mult" }, { "/", "__Div" }, { "%", "__Mod" }, { "**", "__Exponent" }, { "//", "__FloorDiv" },
			//EQUALITY
			{ "==", "__EqualityCompare" }, { "!=", "__NotEqualityCompare" }, { "<>", "__NotEqualityCompare" }, { ">", "__Superior" }, {"<", "__Inferior" }, {">=", "__SuperiorOrEqual" }, {"<=", "__InferiorOrEqual" },
			//ASSIGNATION
			{"=", "__Assign"}, {"+=", "__AddAssign" }, {"-=", "__SubAssign" }, {"*=", "__MultAssign" }, {"/=", "__DivideAssign" }, {"%=", "__ModAssign" }, {"**=", "__ExponentAssign" }, {"//=", "__FloorDivAssign"} };
	

		/*==	If the values of two operands are equal, then the condition becomes true.	(a == b) is not true.
!=	If values of two operands are not equal, then condition becomes true.
<>	If values of two operands are not equal, then condition becomes true.	(a <> b) is true. This is similar to != operator.
>	If the value of left operand is greater than the value of right operand, then condition becomes true.	(a > b) is not true.
<	If the value of left operand is less than the value of right operand, then condition becomes true.	(a < b) is true.
>=	If the value of left operand is greater than or equal to the value of right operand, then condition becomes true.	(a >= b) is not true.
<=	If the value of left operand is less than or equal to the value of right operand, then condition becomes true.	(a <= b) is true.
Python Assignment Operators
Assume variable a holds 10 and variable b holds 20, then −

[ Show Example ]

Operator	Description	Example
=	Assigns values from right side operands to left side operand	c = a + b assigns value of a + b into c
+= Add AND	It adds right operand to the left operand and assign the result to left operand	c += a is equivalent to c = c + a
-= Subtract AND	It subtracts right operand from the left operand and assign the result to left operand	c -= a is equivalent to c = c - a
*= Multiply AND	It multiplies right operand with the left operand and assign the result to left operand	c *= a is equivalent to c = c * a
/= Divide AND	It divides left operand with the right operand and assign the result to left operand	c /= a is equivalent to c = c / ac /= a is equivalent to c = c / a
%= Modulus AND	It takes modulus using two operands and assign the result to left operand	c %= a is equivalent to c = c % a
**= Exponent AND	Performs exponential (power) calculation on operators and assign value to the left operand	c **= a is equivalent to c = c ** a
//= Floor Division	It performs floor division on operators and assign value to the left operand	c //= a is equivalent to c = c // a
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 + Addition	Adds values on either side of the operator.	a + b = 30
- Subtraction	Subtracts right hand operand from left hand operand.	a – b = -10
* Multiplication	Multiplies values on either side of the operator	a * b = 200
/ Division	Divides left hand operand by right hand operand	b / a = 2
% Modulus	Divides left hand operand by right hand operand and returns remainder	b % a = 0
** Exponent	Performs exponential (power) calculation on operators	a**b =10 to the power 20
//	Floor Division - The division of operands where the result is the quotient in which the digits after the decimal point are removed. But if one of the operands is negative, the result is floored, i.e., rounded away from zero (towards negative infinity):	9//2 = 4 and 9.0//2.0 = 4.0, -11//3 = -4, -11.0//3 = -4.0*/
		public static Dictionary<string, List<LanguageToken>> occurences = new Dictionary<string, List<LanguageToken>>();
		public static Dictionary<string, List<LanguageToken>> typeOccurences = new Dictionary<string, List<LanguageToken>>();
		public static Dictionary<string, ScopeDescriptor> rootTypeDescriptors = new Dictionary<string, ScopeDescriptor>();
		public static Dictionary<string, ScopeDescriptor> namespacesDescriptors = new Dictionary<string, ScopeDescriptor>(); // a la fois on met des . a la fois on met des "parentScope" a la fois on met des scopeChildrens
		public int startPosition = 0;
		public int endPosition = 0;
		public int lineNumber = 0;
		public LanguageToken scopeToken = null; // class, etc qui correspond au scopeIN sur lequel on est. (uniquement pour un scopeIN)
		public LanguageToken scopeTokenName = null; //  class "Maurice", qui correspond au scopeIN sur lequel on est, (fonctionne aussi pour les fonctions).
													// pour les "name", ils se changent en "functionParameter", ou "structMember" (que ce soit class/enum/interface), ou "className", 
													// ou "namespaceMember", ou "namespaceName", "varDecl (les ":")" "memberDecl" (variable déclarée dans le scope direct d'une classe), "functionDecl" (fonction non déclarée dans le scope direct d'une classe),  "methodDecl"(fonction declarée dans le scope direct d'une classe), ou "memberCall" (après un . peu importe), functionCall(si appelé dans le scope d'une functionDecl/ou methodDecl), methodCall(après un . mais si le suivant est un funcIN (le const du suivant change le dernier)), functionDeclaration(après rien);

		// changer le dernier suppose aussi d'aller l'enlever des typeOccurences, et l'ajouter a nouveau après. 
		// scopeIn peut eventuellement aller se tagger le truc qui lui correspond vraiment. 
		public string tokenType = ""; // var, name, scope, functionParameters, descriptionkeyword (un peu comme un type mais genre class/enum/namespace), newLine, space_spacement, tab_spacement, FileBegining
		public string tokenString = "";
		public LanguageToken varType = null;
		public Object tag = null; //
		public int lvl
		{
			get
			{
				int lvl = 0;
				LanguageToken current = this;
				while (current.parent != null)
				{
					lvl++;
					current = current.parent;
					;
				}
				return (lvl);
			}
		}
		public StringBuilder textFile = new StringBuilder(); // on insere des tokens de vide quand on remplace par un autre terme. ou on invalide la suite si c'etait plus grand.
		public bool commentedLine = false;
		public bool commentedText = false;
		public int arrayDimension = 0;
		public void Build()
		{
			;
		}
		private string _CSharpTraduction = "";
		public string CSharpTraduction
		{
			get
			{
				return ((string)_CSharpTraduction.Clone());
			}
		}
		public LambdaFunctionDescriptor BuildLambdaFunction(LineInfo info, out LanguageToken next)
		{
			LambdaFunctionDescriptor res = new LambdaFunctionDescriptor("", null);
			List<ParameterDescriptor> parameters = new List<ParameterDescriptor>();
			next = this.next;
			bool action = false;
			next = this.next;
			Console.WriteLine("NEXT?" + next.tokenType);
			//StringBuilder parameters = new StringBuilder("Func");
			LanguageToken current = this.NextLogic;
			while (current.tokenType != "funcOUT")
			{
				current = current.NextLogic;
			}
			current.DebugALittle();
			if (current.tokenType == "funcOUT")
			{
				Console.WriteLine("OK?");
				current = current.NextLogic;
				if (current.tokenType == "lambdaArrow")
				{
					this.lambdaReturnShortcut = current;
					;
				}
			}
			
			if (this.lambdaReturnShortcut.NextLogic.tokenString == "void")
			{
				action = true;
				//parameters = new StringBuilder("Action");
			}


			Console.WriteLine("PARAMETERS!!!!:");
			//this.DebugALittle(5, true);

			bool once = false;
			bool isParameters = false;
			current = this.LastLogic;
			parameters = current.BuildParameters(out current);

			isParameters = parameters.Count > 0;
			Console.WriteLine("after the out:" + current.tokenType);
			current = current.NextLogic;
			TypeDescriptor returnType = null;
			current = current.NextLogic;
			//if (!action)
			//{
				if (once)
					;//parameters.Append(", ");
				returnType = current.virtualTypeName2(null, out current);
				//throw new System.Exception("returnType:" + returnType.typeName);
			
		
			next = current;
			res.isAction = action;
			res.parameters = parameters;
			res.type = returnType;
			//return (parameters.ToString());
			return (res);
		}
		public string CSharpLambdaTraduction(LineInfo info, out LanguageToken next)
		{
			bool action = false;
			//throw new NotImplementedException();
			// retourne les params encapsulés, et l'interieur de la fonction ("throw new NotImplementedException();")
			next = this.next;
			Console.WriteLine("NEXT?" + next.tokenType);
			StringBuilder parameters = new StringBuilder("Func");
			LanguageToken current = this.NextLogic;
			while (current.tokenType != "funcOUT")
			{
				current = current.NextLogic;
			}
			if (current.tokenType == "funcOUT")
			{
				current = current.NextLogic;
				if (current.tokenType == "lambdaArrow")
				{
					this.lambdaReturnShortcut = current;
					;
				}
			}		
			if (this.lambdaReturnShortcut.NextLogic.tokenString == "void")
			{
				action = true;
				parameters = new StringBuilder("Action");
			}


			Console.WriteLine("PARAMETERS!!!!:");
			//this.DebugALittle(5, true);

			bool once = false;
			bool isParameters = false;
			current = this.NextLogic;
			while (current.tokenType != "funcOUT")
			{
				if (current.tokenType == "TYPE" && current.NextLogic.tokenType != "colons")
				{
					if (once)
						parameters.Append(", ");
					else
					{
						isParameters = true;
						parameters.Append("<");
					}
					Console.WriteLine("ADDING PARAMETER:" + current.tokenString);
					if (BuiltinTypes.ContainsKey(current.tokenString))
					{
						parameters.Append(BuiltinTypes[current.tokenString]);
					}
					else
						parameters.Append(current.tokenString);
					once = true;
				}
				current = current.NextLogic;
			}
			Console.WriteLine("after the out:" + current.tokenType);
			current = current.NextLogic;

			current = current.NextLogic;
			if (!action)
			{
				if (once)
					parameters.Append(", ");
				if (BuiltinTypes.ContainsKey(current.tokenString))
				{
					parameters.Append(BuiltinTypes[current.tokenString]);
				}
				else
					parameters.Append(current.tokenString);
			}
			if (isParameters || !action)
				parameters.Append(">");
			current = current.NextLogic;
			next = current;
			return (parameters.ToString());

		}

		public void DebugALittle(int depth = 5, bool typeInfo = false)
		{
			//throw new System.Exception("no");
			//return;
			LanguageToken current = this;
			if (typeInfo)
				Console.WriteLine(this.tokenType);
			else
				Console.WriteLine("this:" + this.tokenString);
			for (int i = 0; i < depth && current.LastLogic != null; i++)
			{
				current = current.LastLogic;
			}
			Console.WriteLine("LAST TOKENS:");
			for (int i = 0; i < depth && current != null; i++)
			{
				if (!typeInfo)
					Console.WriteLine(current.tokenString);
				else
				{
					Console.WriteLine(current.tokenType);
				}
				current = current.NextLogic;
			}
			Console.WriteLine("NEXT TOKENS:");
			for (int i = 0; i < depth && current != null; i++)
			{
				if (!typeInfo)
					Console.WriteLine(current.tokenString);
				else
				{
					Console.WriteLine(current.tokenType);
				}
				current = current.NextLogic;
			}
		}
		public string CSharpTypeExpression(LineInfo info, out LanguageToken next)
		{
			next = this.next;

			return ("");
		}
		public class EvaluationDescriptor : ScopeDescriptor
		{
			public EvaluationDescriptor() : base("", "evaluation", false)
			{

			}
		}
		public class ForAssignationDescriptor: EvaluationDescriptor
		{
			
		}
		public class ForEvaluationDescriptor : EvaluationDescriptor
		{
			///public 
			public ForEvaluationDescriptor(): base()
			{
				LanguageToken token;
				ScopeDescriptor desc = null;
				int i = 0;
				for (i = 0; i < 500 && null != (desc = BuildObjectDescriptor(null, out token)); i++)
				{

				}
			}
		}
		public static ScopeDescriptor BuildObjectDescriptor(LanguageToken token, out LanguageToken next)
		{

			//test testouille = testouilllllle;
			//test2 testouille = test3;
			ScopeDescriptor res = new ScopeDescriptor("", "");
			res.isObjectDescriptor = true;
			List<TypeDescriptor> types = new List<TypeDescriptor>();
			List<LanguageToken> fields = new List<LanguageToken>();
			LanguageToken current = token;
			LanguageToken fake = null;
			Console.WriteLine("HEY?");
			Console.WriteLine("BUILDING OBJECT DESC:" + current.GetTypeTarget().tokenString);
			Console.WriteLine("OYH");
			next = token.next;
			while (current.NextLogic != null && current.tokenType != "scopeOUT")
			{
				if (current.NextLogic.tokenType == "NAME")
				{
					Console.WriteLine("object desc param:" + current.NextLogic.tokenString);
					current.NextLogic.DebugALittle();
					fields.Add(current.NextLogic);
					current.NextLogic.targetType = current.NextLogic.NextLogic;
					string fieldName = current.NextLogic.tokenString;
				
					if (current.NextLogic.NextLogic.tokenType == "funcIN")
					{
						Console.WriteLine("ITS A METHOD:" + fieldName);
						MethodDescriptor desc = current.NextLogic.BuildMethod(current.NextLogic.tokenString, out current);
						Console.WriteLine("AFTER THE METHOD:");
						current.DebugALittle();
						res.AddMethod(desc);
					}
					else
					{
						TypeDescriptor fieldType = current.NextLogic.targetType.virtualTypeName2(null, out current);
						MemberDescriptor member = new MemberDescriptor(fieldName, fieldType, current.NextLogic, current.NextLogic.parent);
						res.SetMember(member);
						Console.WriteLine("CURRENT:");
						current.DebugALittle();
						Console.WriteLine("DONE");
					}
				}
				else if (current.NextLogic.tokenType == "arrayIN")
				{
					// parse indexer here.
					//
					LanguageToken indexerName = current.NextLogic.NextLogic;
					///							{			[		dslnffnl
					LanguageToken indexerType = current.NextLogic.NextLogic.NextLogic.NextLogic;
					///							{			[		fsqqsfsq	:		string			
					LanguageToken returnType = current.NextLogic.NextLogic.NextLogic.NextLogic.NextLogic.NextLogic.NextLogic;
					///							{			[		fsqqsfsq	:		string	 ]			:		string

					IndexerDescriptor indexer;
					indexer = new IndexerDescriptor(indexerType.virtualTypeName2(null, out fake), returnType.virtualTypeName2(null, out fake));
					res.indexers.Add(indexer);
					current = returnType;
				}
				else if (current.NextLogic.tokenType == "colons")
				{
					types.Add(current.virtualTypeName2(null, out current));
				}
				else
				{
					Console.WriteLine("CURRENT TYPE:" + current.tokenType);
					current = current.NextLogic;
				}
			}

			next = current.NextLogic;
			string className = token.GetTypeTarget().tokenString;
			string tmp = className.Capitalize();
			className = tmp + "_" + 0 + "_";
			int k = 1;
			while (declaredTypes.ContainsKey(className))
			{
				//throw new System.Exception("test");
				className = tmp + "_" + k + "_";
				k++;
			}
			declaredTypes.Add(className, token);

			string TypeName = className;
			string trimms = "";
			if (next.tokenType == "semicolon")
			{
				next = next.NextLogic;
			}
			if (next.tokenType == "scopeOUT")
			{
				next = next.NextLogic;
			}
			res.scopeName = TypeName;
			res.scopeType = "classKW";
			res.parentScope = LanguageToken.activeScope;
			if (res.parentScope != null)
			{
				res.parentScope.scopeChildrens.Add(res.scopeName, res);
			}
			if (next != null)
				Console.WriteLine("NEXT AFTER OBJECT DESC:" + next.tokenString);
			Console.WriteLine("DONE OBJECT DESC");
			return (res);
		}
		public List<ParameterDescriptor> BuildParameters(out LanguageToken next)
		{
			List<ParameterDescriptor> parameters = new List<ParameterDescriptor>();
			LanguageToken token = this.NextLogicSkipNewLines;
			next = token.next;
			if (token.parameters == null)
			{
				token.parameters = new List<LanguageToken>();
				Console.WriteLine("params null??");
				token.DebugALittle();
				//throw new System.Exception("ok");
				LanguageToken current = token;
				if (current.tokenType == "funcIN")
				{
					current = current.NextLogicSkipNewLines;
					while (true)
					{
						if (current.tokenType == "infiniteArgs")
						{
							current = current.NextLogicSkipNewLines;
						}
						if (current.tokenType == "NAME")
						{
							Console.WriteLine("PARAM FOUND:" + current.tokenString);
							token.parameters.Add(current);
							
							//current = current.NextLogicSkipNewLines;
							if (current.tokenType == "optionnalParameter")
							{
								current = current.NextLogicSkipNewLines;
							}
							TypeDescriptor type = current.NextLogicSkipNewLines.virtualTypeName2(null, out current);
							//throw new Exception("TYPE:" + type + ", PARAM:" + current.tokenType);
						}
					
						if (current.tokenType == "colons")
						{
							TypeDescriptor type = current.virtualTypeName2(null, out current);
						}
						if (current.tokenType == "comma")
						{
							current = current.NextLogicSkipNewLines;
							;
						}
						else
						{
							//throw new Exception("OY?");
							current = current.NextLogicSkipNewLines;
							break;
						}
					}
				}
			
			}
			for (int i = 0; i < token.parameters.Count; i++)
			{
				if (token.parameters[i].tokenType == "optionnalParameter")
				{
					token.parameters[i] = token.parameters[i].LastLogic;
				}
			}

			for (int i = 0; i < token.parameters.Count; i++)
			{
				bool isOptionnal = false;
				TypeDescriptor virtualTypeName = null;
				string varName = "";
				StringBuilder parameterString = new StringBuilder("");
				bool optionnal = false;
				bool isInfinite = false;
				if (token.parameters[i].LastLogic.tokenType == "infiniteArgs")
				{
					isInfinite = true;
				}
				if (token.parameters[i].NextLogic.tokenType == "optionnalParameter")
				{
					optionnal = true;
					if (token.parameters[i].NextLogic.NextLogic.tokenType == "colons")
					{
						Console.WriteLine("READING COLONS FOR:" + token.parameters[i].tokenString);
						virtualTypeName = token.parameters[i].NextLogic.NextLogic.NextLogic.virtualTypeName2(null, out next);

					}
					else
					{
						virtualTypeName = new TypeDescriptor("object");
					}
				}
				else if (token.parameters[i].NextLogic.tokenType == "colons")
				{
					Console.WriteLine("READING COLONS FOR:" + token.parameters[i].tokenString);
					virtualTypeName = token.parameters[i].NextLogic.virtualTypeName2(null, out next);
					Console.WriteLine("AFTER COLONS");
					
				}
				else
				{
					virtualTypeName = new TypeDescriptor("object");
				}

				varName = token.parameters[i].formatedVarName;
				Console.WriteLine("FOUND PARAMETER:" + varName);
				if (optionnal)
				{
					isOptionnal = true;
					parameterString.Append(" = " + "default(" + virtualTypeName + ")");
				}
				if (next.tokenType == "arrayIN")
				{
					next = next.NextLogic;
					if (next.tokenType == "arrayOUT")
					{
						next = next.NextLogic;
					}
				}
				if (next.tokenType == "space_spacement" || next.tokenType == "tab_spacement")
				{
					next = next.NextLogic;
				}
				ParameterDescriptor param = new ParameterDescriptor(varName, virtualTypeName, token);
				param.isOptionnal = isOptionnal;
				param.isInfinite = isInfinite;
				//param.text = parameterString.ToString();
				parameters.Add(param);
			}
			return (parameters);
		}
		public MethodDescriptor BuildMethod(string name, out LanguageToken next, bool lambda = false)
		{
			bool isStatic = false;
			if (this.LastLogic.tokenType == "staticKW")
			{
				Console.WriteLine("FUNCTION:" + name + "IS STATIC????");
				isStatic = true;
				//throw new System.Exception("isSTATIC");
			}
			LanguageToken current = this;
			LanguageToken token = this.NextLogic;
			MethodDescriptor res = new MethodDescriptor(name, null, token, token.parent);

			List<GenericTypeParameterDescriptor> genericParameters = new List<GenericTypeParameterDescriptor>();
			if (this.NextLogic.tokenType == "typeIN")
			{
				Console.WriteLine("found type in");
				current = this.NextLogic.NextLogic;
				while (current.tokenType == "TYPE")
				{
					TypeDescriptor extends = null;
					string paramName = current.tokenString;
					Console.WriteLine("name:" + paramName);
					if (current.NextLogic.tokenType == "extendsKW")
					{
						extends = current.NextLogic.NextLogic.virtualTypeName2(null, out current);
						throw new System.Exception("EXTENDS:" + extends);
					}
					else
					{
						current = current.NextLogic;
					}
					genericParameters.Add(new GenericTypeParameterDescriptor(paramName, extends));
					if (current.tokenType != "comma")
					{
						break;
					}
					else
					{
						Console.WriteLine("was comma");
						current = current.NextLogic;
					}
				}
			}
			List<ParameterDescriptor> parameters = current.BuildParameters(out next);
			res.isStatic = isStatic;
			res.parameters = parameters;
			res.genericParameters = genericParameters;
			if (next.NextLogic.tokenType == "funcOUT")
			{
				next = next.NextLogic;
				Console.WriteLine("FUNCOUT FOUND");
			}
			next.DebugALittle();
			Console.WriteLine("next type:" + next.NextLogic.tokenType);
			TypeDescriptor typeName = next.NextLogic.virtualTypeName2(null, out next);
			Console.WriteLine("METHOD TYPE:" + typeName);
			Console.WriteLine("METHOD TYPE DEBUG A LITTLE");
			next.NextLogic.DebugALittle();
			res.type = typeName;
			if (token.LastLogic.LastLogic.tokenType == "staticKW" || token.LastLogic.tokenType == "staticKW")
			{
				Console.WriteLine("STATIC FROM HERE");
				res.isStatic = true;
			}
			Console.WriteLine("NEXT AFTER THE FUNCTION:" + next.tokenType);
			next = next.next;
			return (res);
		}
		public FunctionDescriptor BuildFunction(string name, out LanguageToken next, bool lambda = false)
		{
			LanguageToken current = this;
			LanguageToken token = this;
			FunctionDescriptor res = new FunctionDescriptor(name, null, this, this.parent);
			next = this.next;
			List<GenericTypeParameterDescriptor> genericParameters = new List<GenericTypeParameterDescriptor>();
			if (this.NextLogic.tokenType == "typeIN")
			{
				current = this.NextLogic.NextLogic;
				while (current.tokenType == "TYPE")
				{
					TypeDescriptor extends = null;
					string paramName = current.tokenString;
					Console.WriteLine("name:" + paramName);
					if (current.NextLogic.tokenType == "extendsKW")
					{
						extends = current.NextLogic.NextLogic.virtualTypeName2(null, out current);
					}
					else
					{
						current = current.NextLogic;
					}
					genericParameters.Add(new GenericTypeParameterDescriptor(paramName, extends));
					if (current.tokenType != "comma")
					{
						break;
					}
					else
					{
						Console.WriteLine("was comma");
						current = current.NextLogic;
					}
				}
			}

			List<ParameterDescriptor> parameters = current.BuildParameters(out next);
			res.parameters = parameters;
			if (next.NextLogic.tokenType == "funcOUT")
			{
				next = next.NextLogic.NextLogic;
			}
			TypeDescriptor typeName = next.NextLogic.virtualTypeName2(null, out next);
			res.type = typeName;
			res.genericParameters = genericParameters;
			Console.WriteLine("NEXT AFTER THE FUNCTION:" + next.tokenType);
			next = next.next;
			return (res);
		}

		public ConstructorDescriptor BuildConstructor(out LanguageToken next)
		{
			LanguageToken token = this;
			ConstructorDescriptor res = new ConstructorDescriptor(this, this.parent, activeScope);
			next = this.next;

			List<ParameterDescriptor> parameters = this.BuildParameters(out next);
			Console.WriteLine("========================AFTER PARAMETERS:");
			next.DebugALittle();
			res.parameters = parameters;
			if (next.NextLogic.tokenType == "funcOUT")
			{
				next = next.NextLogic.NextLogic;
			}
			//TypeDescriptor typeName = next.NextLogic.virtualTypeName2(null, out next);
			//res.type = typeName;
			Console.WriteLine("NEXT AFTER THE FUNCTION:" + next.tokenType);
			next = next.next;
			return (res);
		}
		public MemberDescriptor BuildMember(string name, out LanguageToken next)
		{
			bool isStatic = false;
			TypeDescriptor typeName = null;
			if (this.LastLogic.tokenType == "staticKW")
			{
				isStatic = true;
			}
			MemberDescriptor res = new MemberDescriptor(name, null, this, this.parent);
			if (this.NextLogic.tokenType == "comma" || this.NextLogic.tokenType == "scopeOUT")
			{
				typeName = null;
				next = this.NextLogic;
			}
			else
				typeName = this.NextLogic.virtualTypeName2(new LineInfo(), out next);
			Console.WriteLine("TYPENAME:" + typeName);
			if (this.parent.scopeToken.tokenType != "enumKW")
			{
				if (this.LastLogic.LastLogic.tokenType == "staticKW" || this.LastLogic.tokenType == "staticKW")
					res.isStatic = true;
				if (typeName == null)
				{
					typeName = new TypeDescriptor("object");
				}
			}
			else
			{
				res.enumMember = true;
			}
			res.isStatic = isStatic;
			res.type = typeName;
			return (res);
		}
		public ValueDescriptor BuildValue(string name, out LanguageToken next)
		{
			ValueDescriptor res = new ValueDescriptor(name, null);
			next = this.next;
			TypeDescriptor typeName = null;
			;
			if (this.LastLogic.tokenType == "constKW")
			{
				res.isConstant = true;
				if (this.NextLogic.tokenType == "colons")
				{
					Dictionary<string, int> @enum = null;
					if ((activeScope.scopeChildrens.ContainsKey(this.NextLogic.NextLogic.tokenString) && activeScope.scopeChildrens[this.NextLogic.NextLogic.tokenString].isEnum))
					{
						@enum = activeScope.scopeChildrens[this.NextLogic.NextLogic.tokenString].enumMembers;
						@enum.Add(this.tokenString, activeScope.scopeChildrens[this.NextLogic.NextLogic.tokenString].enumMembers.Keys.Count);
					}
					else if ((activeScope.namespaceChildrens.ContainsKey(this.NextLogic.NextLogic.tokenString) && activeScope.namespaceChildrens[this.NextLogic.NextLogic.tokenString].isEnum))
					{
						@enum = activeScope.namespaceChildrens[this.NextLogic.NextLogic.tokenString].enumMembers;
						@enum.Add(this.tokenString, activeScope.namespaceChildrens[this.NextLogic.NextLogic.tokenString].enumMembers.Keys.Count);
					}
					if (@enum != null)
					{
						LanguageToken varName = this;
						next = this.NextLogic;
						if (varName.NextLogic.tokenType == "equal")
						{

							@enum[varName.tokenString] = Int32.Parse(varName.NextLogic.NextLogic.tokenString);
							next = varName.NextLogic.NextLogic.NextLogic.NextLogic;
						}
						else
						{

							//this.values.Add(varName.tokenString, this.values.Keys.Count);
							if (varName.NextLogic.tokenType == "comma")
							{
								next = varName.NextLogic.NextLogic;
								;
							}
							else if (varName.NextLogic.tokenType == "scopeOUT")
							{
								next = varName.NextLogic;
							}
							else if (varName.NextLogic.tokenType == "colons")
							{
								next = varName.NextLogic.NextLogic.NextLogic.NextLogic;
								;
							}
						}
					}
					//registeredEnums[this.NextLogic.NextLogic.tokenString].AddValue(this, out next);

				}
			}
			Console.WriteLine("VAR NAME FOUND" + this.tokenString);
			Console.WriteLine("Sould be typename?" + this.NextLogic.tokenType);
			if (this.NextLogic.tokenType == "comma" || this.NextLogic.tokenType == "scopeOUT")
			{
				typeName = null;
				next = this.NextLogic;
			}
			else
				typeName = this.NextLogic.virtualTypeName2(new LineInfo(), out next);
			res.type = typeName;
			return (res);
		}

	/*	public string CSharpFunctionDefinition(LineInfo info, out LanguageToken next, bool isMethod = false, bool lambda = false)
		{

			//throw new NotImplementedException();
			// retourne les params encapsulés, et l'interieur de la fonction ("throw new NotImplementedException();")
			next = this.next;
			Console.WriteLine("NEXT?" + next.tokenType);
			StringBuilder parameters = new StringBuilder("Func<");
			if (!lambda)
				parameters = new StringBuilder("(");
			if (this.parameters == null)
			{
				Console.WriteLine("PARAMS NULL");
				//this.DebugALittle();
			}
			Console.WriteLine("PARAMETERS!!!!:");
			//this.DebugALittle(5, true);
			if (this.parameters == null)
				this.parameters = new List<LanguageToken>();
			for (int i = 0; i < this.parameters.Count; i++)
			{
				if (this.parameters[i].tokenType == "optionnalParameter")
				{
					this.parameters[i] = this.parameters[i].LastLogic;
				}
				//Console.WriteLine(this.parameters[i].tokenType);
			}
			//Console.WriteLine("END!!!!!");
			for (int i = 0; i < this.parameters.Count; i++)
			{
				string virtualTypeName = "";
				bool optionnal = false;
				/*if (this.parameters[i] == null)
				{
					Console.WriteLine("WTTFFFF???");
				}
					Console.WriteLine("PARAMETER:" + this.parameters[i].tokenType + ";;; name:" + this.parameters[i].tokenString);

				*/
				/*
				if (this.parameters[i].NextLogic.tokenType == "optionnalParameter")
				{
					//Console.WriteLine("BAH???");
					if (this.parameters[i].NextLogic.NextLogic.tokenType == "colons")
					{
						optionnal = true;
						//Console.WriteLine("BEEEHHH???");
						virtualTypeName = this.parameters[i].NextLogic.NextLogic.NextLogic.virtualTypeName(info, out next);
						Console.WriteLine("TYPE NAME FOUND IN FUNCTION PARAMETER:" + virtualTypeName);
						parameters.Append(virtualTypeName);

						Console.WriteLine("NEXT AFTER TYPE OF PARAMETER" + next.tokenType);
						Console.WriteLine("FOR THE LAST TYPE:" + this.parameters[i].NextLogic.NextLogic.NextLogic.tokenString);
						////////
					}
					else
					{
						parameters.Append("Object");
					}
				}
				else if (this.parameters[i].NextLogic.tokenType == "colons")
				{
					virtualTypeName = this.parameters[i].NextLogic.virtualTypeName(info, out next);
					if (this.parameters[i].LastLogic.tokenType == "infiniteArgs")
					{
						parameters.Append("params ");
					}
					parameters.Append(virtualTypeName);

					Console.WriteLine("VirtualTypeName:" + virtualTypeName);
					Console.WriteLine("Accessing virtual type name for:" + this.parameters[i].NextLogic.tokenType);
					//Console.WriteLine("NEXT0?" + next.tokenType);
					if (next.tokenType == "arrayIN")
					{

					}
					Console.WriteLine("FOR THE LAST TYPE:" + this.parameters[i].NextLogic.NextLogic.tokenString);
					//Console.WriteLine("Virtual Type:" + this.parameters[i].NextLogic.virtualTypeName);
				}
				if (!lambda)
				{



					parameters.Append(" " + this.parameters[i].formatedVarName);
					if (optionnal)
					{
						parameters.Append(" = " + "default(" + virtualTypeName + ")");
					}

				}
				if (next.tokenType == "arrayIN")
				{
					next = next.NextLogic;
					if (next.tokenType == "arrayOUT")
					{
						next = next.NextLogic;
					}
				}
				if (next.tokenType == "space_spacement" || next.tokenType == "tab_spacement")
				{
					next = next.NextLogic;
				}
				//Console.WriteLine("ACCESSING VIRTUAL TYPE NAME for:" + this.parameters[i].tokenType);
				//parameters.Append(this.parameters[i].virtualTypeName);
				//Console.WriteLine("virtual type name:" + this.parameters[i].virtualTypeName);
				if (i + 1 < this.parameters.Count)
					parameters.Append(", ");
			}
			Console.WriteLine("NEXT?" + next.tokenType);

			string LambdaReturn = "";
			//Console.WriteLine(this.parameters[this.parameters.Count - 1].tokenType);
			/*if (next.tokenType == "lambdaArrow" || next.tokenType == "funcIN")
				Console.WriteLine("WIIIIWIIIII");
			else
			{
				Console.WriteLine("NEXT?" + next.tokenType);
			}*/


			//if (next.NextLogic)

			/*while (!lambda && current.tokenType != "funcOUT")
			{
			
				if (current == null || current.NextLogic == null)
				{
					DebugALittle2();
					throw new System.Exception("WTF:");
				}
				if (current.NextLogic.tokenType == "funcOUT" && current.parent != this)
				{
					if (current.parent != this)
					{
						Console.WriteLine("THIS TOKEN TYPE:" + this.tokenType);
						Console.WriteLine((current.parent.tokenType));
					}
					current = current.NextLogic.NextLogic;
				}
				else
				{
					current = current.NextLogic;
				}
			}*/
			/*if (current.parent == this)
			{
				Console.WriteLine("ya ok");
			}
			next = current.next;*//*
			parameters.Append(")");
			string functionIn = @"{
throw new System.NotImplementedException();
}";
			int lineNb = 0;
			string[] test = functionIn.Split('\n');
			int lvl = this.lvl;
			StringBuilder functionIN2 = new StringBuilder("\n");
			for (int i = 0; i < test.Length; i++)
			{
				int mod = 1;
				if (i == 0 || i == test.Length - 1)
				{
					mod = 0;
				}
				for (int j = 0; j < lvl + mod; j++)
				{
					functionIN2.Append("\t");
				}
				functionIN2.Append(test[i]);
				functionIN2.Append("\n");
			}
			functionIn = functionIN2.ToString();
			StringBuilder res = parameters;
			if (!lambda)
			{
				res.Append(functionIn);
			}
			else
			{
				;
			}
			Console.WriteLine("DONE READ FUNCTION");
			return (res.ToString());
		}*/
		public LanguageToken GetTypeTarget()
		{

			LanguageToken current = this.LastLogic;
			while (current != null) //&& current.typeTarget != null)
			{
				if (current.tokenType == "colons")
				{
					current = current.LastLogic;
					if (current.tokenType == "optionnalParameter")
					{
						current = current.LastLogic;
					}
					if (current.tokenType == "funcOUT")
					{
						while (current.tokenType != "funcIN")
						{
							current = current.LastLogic;
						}
						return (current.LastLogic);
					}
					return (current);
				}
				current = current.LastLogic;
				Console.WriteLine("Hey?");
			}
			if (current == null)
				Console.WriteLine("heyoh?");
			if (this.tokenType == "colons")
			{
				current = this.LastLogic;
			}
			else if (current == null)
			{
				Console.WriteLine("MISSING TARGET FOR:" + this.tokenType);
			}
			return (current);
		}

		public class CSharpEnumDefinition
		{
			public LanguageToken startToken = null;
			public Dictionary<string, int> values = new Dictionary<string, int>();
			public string enumName = "";
			public StringBuilder stringObject
			{
				get
				{
					bool inNamespace = false;
					StringBuilder res = new StringBuilder();

					res.Append("public enum " + this.enumName + "\n{\n");
					int k = 0;
					foreach (var key in this.values.Keys)
					{
						res.Append(key);
						res.Append(" = ");
						res.Append("" + this.values[key]);
						if (k + 1 < this.values.Keys.Count)
						{
							res.Append(",");
						}
						res.Append("\n");
						k++;
					}
					res.Append("}\n");

					return (res);
				}
			}
			public static CSharpEnumDefinition FromToken(LanguageToken token, out LanguageToken next)
			{
				CSharpEnumDefinition self = new CSharpEnumDefinition();
				LanguageToken current = token;
				self.startToken = token;
				next = current.next;
				self.enumName = token.NextLogic.tokenString;
				registeredEnums[self.enumName] = self;

				Console.WriteLine("REGISTERING ENUM:" + self.enumName);
				next = current.NextLogic.NextLogic;
				while (current.tokenType != "scopeOUT")
				{
					if (nameKinds.ContainsKey(current.tokenType))
					{
						self.AddValue(current, out next);
					}
					current = current.NextLogic;
				}
				next = current.NextLogic;
				if (next.tokenType == "semicolon")
				{
					next = next.NextLogic;
				}
				return (self);
				;
			}
			public void AddValue(LanguageToken varName, out LanguageToken next)
			{
				next = varName.NextLogic;
				if (varName.NextLogic.tokenType == "equal")
				{
					this.values.Add(varName.tokenString, Int32.Parse(varName.NextLogic.NextLogic.tokenString));
					next = varName.NextLogic.NextLogic.NextLogic.NextLogic;
				}
				else
				{
					this.values.Add(varName.tokenString, this.values.Keys.Count);
					if (varName.NextLogic.tokenType == "comma")
					{
						next = varName.NextLogic.NextLogic;
						;
					}
					else if (varName.NextLogic.tokenType == "scopeOUT")
					{
						next = varName.NextLogic;
					}
					else if (varName.NextLogic.tokenType == "colons")
					{
						next = varName.NextLogic.NextLogic.NextLogic.NextLogic;
						;
					}
				}
				;
			}
		}

		public static LanguageToken ActiveScope = null;
		private LanguageToken nextToParse = null;
		public string builtinType = "";
		private string CSharpLambdaString = "";
		public LambdaFunctionDescriptor lambdaDesc = null;
		private LanguageToken nextAfterVirtualTypeName = null;
		public TypeDescriptor virtualTypeName2(LineInfo info, out LanguageToken next, bool firstPass = true)
		{
			next = this.next;
			if (firstPass)
			{
				TypeDescriptor typeName = this.virtualTypeName2(info, out next, false);
				if (next.tokenType == "or")
				{
					Console.WriteLine("OR FOUND");
					AnyTypeDescriptor res = new AnyTypeDescriptor(new List<TypeDescriptor>(), null);
					//typeName = "Any<" + typeName;
					res.types.Add(typeName);
					while (next.tokenType == "or")
					{
						next = next.NextLogic;
						TypeDescriptor tmp = next.virtualTypeName2(info, out next, false);
						Console.WriteLine("NEXT TYPE:" + tmp);
						//throw new System.Exception("NEXT TYPE:" + tmp);
						res.types.Add(tmp);
					}
					//throw new System.Exception("RES TO STRING:" + res.ToString());
					return (res);
				}
				Console.WriteLine("TYPENAME:" + typeName);
				return (typeName);
			}
			Console.WriteLine("virtual type nameee:" + this.tokenType);
			next = this.next;
			if (this.builtinType == "" && this.tokenType == "TYPE" && BuiltinTypes.ContainsKey(this.tokenString))
			{

				this.builtinType = BuiltinTypes[this.tokenString];
			}
			if (this.tokenType == "optionnalParameter")
			{
				return (this.NextLogic.virtualTypeName2(info, out next));
			}
			if (this.tokenType == "genericTYPE")
			{

				if (this.tokenString == "Array")
				{
					Console.WriteLine("ARRAY FOUND");
					next = this.NextLogic;
					var typeIn = next.NextLogic.virtualTypeName2(null, out next);
					next = next.next;
					typeIn.dimension++;
					return (typeIn);
				}
				else
				{
					next = this.NextLogic;
					GenericTypeDescriptor res = new GenericTypeDescriptor(new TypeDescriptor(this.tokenString), null, null);
					Console.WriteLine("GENERIC TYPE:" + this.tokenString);
					List<TypeDescriptor> parameters = new List<TypeDescriptor>();
					StringBuilder resType = new StringBuilder();
					if (next.tokenType == "typeIN")
					{
						next = next.NextLogic;
						while (next.tokenType != "typeOUT")
						{
							TypeDescriptor typeName = next.virtualTypeName2(null, out next);
							Console.WriteLine("GENERIC TYPE PARAM FOUND:" + typeName);
							parameters.Add(typeName);
							if (next.tokenType == "comma")
							{
								next = next.NextLogic;
							}
						}
					}
					if (next.tokenType == "typeOUT")
					{
						next = next.NextLogic;
					}
					//while (this.NextLogic. != )
					res.parameters = parameters;
					return (res);
				}
			}
			if (this.tokenType == "TYPE")
			{
				Console.WriteLine("its a type");
				int dimension = 0;
				if (this.NextLogic.tokenType == "dot")
				{
					List<string> types = new List<string>();
					types.Add(this.tokenString);
					Console.WriteLine("OR FOUND");
					next = this.NextLogic.NextLogic;
					while (next.tokenType == "TYPE")
					{
						if (next.tokenType == "TYPE")
						{
							types.Add(next.tokenString);
							next = next.NextLogic;
						}
						if (next.tokenType == "dot" || next.tokenType == "or")
							next = next.NextLogic;
					}
					next = next.next;
					CompoundTypeDescriptor res = new CompoundTypeDescriptor(types);
					return (res);
				}

				if (this.NextLogic.tokenType == "arrayIN")
				{
					;

					LanguageToken current = this.NextLogic;
					while (current.tokenType == "arrayIN")
					{
						current = current.NextLogic;
						if (current.tokenType == "arrayOUT")
						{
							dimension++;
							current = current.NextLogic;
						}

					}

					next = current;
				}
				if (this.builtinType != "")
				{
					if (dimension == 0)
						next = this.NextLogic;
				
					return new TypeDescriptor(this.builtinType, null, dimension);
				}
				else
				{
					Console.WriteLine("it goes here");
					if (dimension == 0)
						next = this.NextLogic;
					return (new TypeDescriptor(this.tokenString, null, dimension));
				}


			}
			if (this.tokenType == "funcIN")
			{
				if (this.lambdaDesc != null)
				{
					Console.WriteLine("LAMBDA EXPRESSION EXIST");
					next = nextAfterVirtualTypeName;
					return (this.lambdaDesc);
				}
				else
				{
					this.lambdaDesc = this.BuildLambdaFunction(info, out next);
					nextAfterVirtualTypeName = next;
					Console.WriteLine("DONE READ LAMBDA EXPRESSION");
					return (this.lambdaDesc);

				}
			}
			else if (this.tokenType == "colons")
			{
				Console.WriteLine("ColonsFound, next?" + this.NextLogic.tokenType);
				Console.WriteLine("NEXT STRING:" + this.NextLogic.tokenString);
				TypeDescriptor virtualTypeName = this.NextLogic.virtualTypeName2(info, out next);
				this.nextAfterVirtualTypeName = this.NextLogic.nextAfterVirtualTypeName;
				return (virtualTypeName);
			}
			else if (builtinType != "")
				return (new TypeDescriptor(builtinType));
			else
			{
				if (this.tokenType == "scopeIN")
				{
					//throw new Exception("???");
					Console.WriteLine("found object descriptor!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
					ScopeDescriptor scope = BuildObjectDescriptor(this, out next);
					//throw new System.Exception("ok");
					//complexTypeRepresentation = CSharpObjectDefinition.FromToken(this, new LineInfo(), out next);
					StringBuilder array = new StringBuilder("");
					if (next.tokenType == "arrayIN")
					{
						;

						while (next.tokenType == "arrayIN" || next.tokenType == "arrayOUT")
						{
							array.Append(next.tokenString);
							next = next.NextLogic;
						}
					}
					nextAfterVirtualTypeName = next;
					return (new TypeDescriptor(scope.scopeName + array.ToString()));
				}
				else if (this.tokenType == "TYPE")
				{
					// ATTENTION NON TABLO.
					throw new System.NotImplementedException();
					ScopeDescriptor scope = BuildObjectDescriptor(this, out next);

					Console.WriteLine("OBJECT DESCT next:" + next.tokenString);
					//complexTypeRepresentation = CSharpObjectDefinition.FromToken(this, info, out next);
					nextAfterVirtualTypeName = next;
				}
				else
				{
					nextAfterVirtualTypeName = this.NextLogic;
				}

				//this.ToCsharpRepresentation(info, out next);
			}
			return (new TypeDescriptor("object"));
			;
		}
		// attention devrait trigger la mise en forme d'un lambda si lambda il y a.
		public string virtualTypeName(LineInfo info, out LanguageToken next, bool firstPass = true)
		{
			if (firstPass)
			{
				string typeName = this.virtualTypeName(info, out next, false);
				if (next.tokenType == "or")
				{
					Console.WriteLine("OR FOUND");
					typeName = "Any<" + typeName;
					while (next.tokenType == "or")
					{
						next = next.NextLogic;
						string tmp = next.virtualTypeName(info, out next, false);
						Console.WriteLine("NEXT TYPE:" + tmp);
						typeName += ", " + tmp;
					}
					typeName += ">";
				}
				Console.WriteLine("TYPENAME:" + typeName);
				return (typeName);
			}
			Console.WriteLine("virtual type nameee:" + this.tokenType);
			next = this.next;
			if (this.builtinType == "" && this.tokenType == "TYPE" && BuiltinTypes.ContainsKey(this.tokenString))
			{

				this.builtinType = BuiltinTypes[this.tokenString];
			}
			if (this.tokenType == "optionnalParameter")
			{
				return (this.NextLogic.virtualTypeName(info, out next));
			}
			if (this.tokenType == "genericTYPE")
			{

				if (this.tokenString == "Array")
				{
					Console.WriteLine("ARRAY FOUND");
					next = this.NextLogic;
					var typeIn = next.NextLogic.virtualTypeName(null, out next);
					next = next.next;
					return (typeIn + "[]");
				}
				else
				{
					//Console.WriteLine("ARRAY FOUND");
					next = this.NextLogic;
					StringBuilder resType = new StringBuilder();
					if (next.tokenType == "typeIN")
					{
						next = next.NextLogic;
						while (next.tokenType != "typeOUT")
						{
							//Console.WriteLine("typename:");
							//next.DebugALittle();
							//Console.WriteLine("done");
							string typeName = next.virtualTypeName(null, out next);
							Console.WriteLine("GENERIC TYPE PARAM FOUND:" + typeName);
							resType.Append(typeName);

							if (next.tokenType != "typeOUT")
							{
								resType.Append(", ");
							}
							if (next.tokenType == "comma")
							{
								next = next.NextLogic;
							}
						}
					}
					if (next.tokenType == "typeOUT")
					{
						next = next.NextLogic;
					}
					//while (this.NextLogic. != )

					return (this.tokenString + "<" + resType.ToString() + ">");
				}
			}
			if (this.tokenType == "TYPE")
			{
				Console.WriteLine("its a type");
				string arrayDesc = "";
				if (this.NextLogic.tokenType == "dot")
				{
					List<string> types = new List<string>();
					Console.WriteLine("OR FOUND");
					next = this.NextLogic.NextLogic;
					while (next.tokenType == "TYPE")
					{
						if (next.tokenType == "TYPE")
						{
							types.Add(next.tokenString);
							next = next.NextLogic;
						}
						if (next.tokenType == "dot" || next.tokenType == "or")
							next = next.NextLogic;
					}
					next = next.next;
					StringBuilder res = new StringBuilder(this.tokenString);
					res.Append(".");
					for (int i = 0; i < types.Count; i++)
					{
						res.Append(types[i]);
						if (i + 1 < types.Count)
							res.Append(".");
					}
					return (res.ToString());
				}

				if (this.NextLogic.tokenType == "arrayIN")
				{
					;

					LanguageToken current = this.NextLogic;
					while (current.tokenType == "arrayIN" || current.tokenType == "arrayOUT")
					{
						arrayDesc += current.tokenString;
						current = current.NextLogic;

					}
					
					next = current;
					Console.WriteLine("TYPE: " + this.tokenString + arrayDesc + "; NEXT:::::" + next.tokenType);
					//throw new System.Exception("NEXT:" + next.tokenType);
				}
				if (this.builtinType != "")
				{
					Console.WriteLine("TYPE: " + this.builtinType + arrayDesc + "; NEXT:::::" + next.tokenType);
					//Console.WriteLine("it goes here builtin type:" + this.tokenString);
					if (arrayDesc == "")
						next = this.NextLogic;
					
					return this.builtinType + arrayDesc;
				}
				else
				{
					Console.WriteLine("it goes here");
					if (arrayDesc == "")
						next = this.NextLogic;
					//Console.WriteLine("next logic:" + this.NextLogic.tokenType);

					return (this.tokenString + arrayDesc);
				}


			}
			//res.Append(this.builtinType);
			if (this.tokenType == "funcIN")
			{
				Console.WriteLine("READING LAMBDA EXPRESSION");
				//throw new System.Exception("LAMBDA FOUND");
				if (this.CSharpLambdaString != "")
				{
					Console.WriteLine("LAMBDA EXPRESSION EXIST");
					next = nextAfterVirtualTypeName;
					return (this.CSharpLambdaString);
				}
				else
				{
					this.CSharpLambdaString = this.CSharpLambdaTraduction(new LineInfo(), out next);
					//this.CSharpLambdaString = this.CSharpFunctionDefinition(new LineInfo(), out next, false, true);
					nextAfterVirtualTypeName = next;
					Console.WriteLine("DONE READ LAMBDA EXPRESSION");
					return (this.CSharpLambdaString);
					
				}
				// mise en fore lambda avant de retourner un truc qu'on stoque ici.
			}
			else if (this.tokenType == "colons")
			{
				Console.WriteLine("ColonsFound, next?" + this.NextLogic.tokenType);
				Console.WriteLine("NEXT STRING:" + this.NextLogic.tokenString);
				string virtualTypeName = this.NextLogic.virtualTypeName(info, out next);
				this.nextAfterVirtualTypeName = this.NextLogic.nextAfterVirtualTypeName;
				return (virtualTypeName);
			}
			else if (builtinType != "")
				return (builtinType);
			else
			{
				if (this.tokenType == "genericType")
				{

					string typeName = this.CSharpGenericType(new LineInfo(), out next);
					this.nextAfterVirtualTypeName = next;
					return (typeName);
				}
				else if (this.tokenType == "scopeIN")
				{
					//throw new Exception("???");
					Console.WriteLine("found object descriptor!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
					ScopeDescriptor scope = BuildObjectDescriptor(this, out next);
					//throw new System.Exception("ok");
					//complexTypeRepresentation = CSharpObjectDefinition.FromToken(this, new LineInfo(), out next);
					StringBuilder array = new StringBuilder("");
					if (next.tokenType == "arrayIN")
					{
						;

						while (next.tokenType == "arrayIN" || next.tokenType == "arrayOUT")
						{
							array.Append(next.tokenString);
							next = next.NextLogic;
						}
					}
					nextAfterVirtualTypeName = next;
					return (scope.scopeName + array.ToString());
				}
				else if (this.tokenType == "TYPE")
				{
					// ATTENTION NON TABLO.
					throw new System.NotImplementedException();
					ScopeDescriptor scope = BuildObjectDescriptor(this, out next);

					Console.WriteLine("OBJECT DESCT next:" + next.tokenString);
					//complexTypeRepresentation = CSharpObjectDefinition.FromToken(this, info, out next);
					nextAfterVirtualTypeName = next;
				}
				else
				{
					nextAfterVirtualTypeName = this.NextLogic;
				}

				//this.ToCsharpRepresentation(info, out next);
			}
			return ("object");
			//return (complexTypeRepresentation.TypeName);
		}
		//	public static List<CSharpFunctionDefintion> pendingGlobals = new List<CSharpFunctionDefintion>();
		public static Dictionary<string, LanguageToken> declaredTypes = new Dictionary<string, LanguageToken>();
		public List<LanguageToken> GetCurrentNamespace()
		{
			LanguageToken current = this.parent;

			List<LanguageToken> res = new List<LanguageToken>();
			while (current != null)
			{
				res.Add(current);
				current = current.parent;
			}
			res.Reverse();


			return (res);
		}
		public string CSharpGenericType(LineInfo info, out LanguageToken next)
		{
			StringBuilder sb = new StringBuilder();

			bool isArray = false;
			if (this.tokenString == "Array")
			{
				isArray = true;
			}

			next = this.next;
			if (this.NextLogic.tokenType == "typeIN" && this.NextLogic.NextLogic.tokenType == "funcIN")
			{


				List<LanguageToken> parameters = new List<LanguageToken>();
				LanguageToken current = this.NextLogic.NextLogic;
				while (current.tokenType != "typeOUT" && current.parent != this.NextLogic)
				{
					LanguageToken tmp;
					if (current.virtualTypeName(info, out tmp) != "")
					{
						parameters.Add(current);
						current = tmp;
					}
					else
					{
						current = current.NextLogic;
					}

				}


			}
			/*
			 disparaitre les parentheses:
			 (( => (
			 <( => <
			  <(( => <

		<	 ( ou )  dans >
		: (( => debut lambda mais faut remplacer le deuxieme par du rien.
			 */
			return (sb.ToString());
		}
		public static void ParseCSharpEnum(LanguageToken token, out LanguageToken next)
		{
			next = token.next;

			;
		}
		public static string FileBegining = @"
		using System;
		using System.Collections.Generic;
		using System.Linq;
		using System.Text;
		using System.Threading.Tasks;
		using Bridge;
		using Bridge.Html5;

		public interface ArrayLike<T>
		{
	
		}
	

		";
		public static Dictionary<string, CSharpEnumDefinition> registeredEnums = new Dictionary<string, CSharpEnumDefinition>();
		//public Array<float test;
		public static bool isTemporaryObject = false;
		public static Dictionary<string, string> BuiltinTypes = new Dictionary<string, string>() {
		{"boolean", "bool"},
		{"number", "float"},
		{"string", "string"},
		{"Array", "Array"},
		{"any", "object"},
		{"Function", "Function"},
		{"ArrayBuffer", "Bridge.Html5.ArrayBuffer"},
		{"ArrayBufferView", "Bridge.Html5.ArrayBufferView"},
		{"DataView", "Bridge.Html5.DataView"},
		{"Float32Array", "Bridge.Html5.Float32Array"},
		{"Float64Array", "Bridge.Html5.Float64Array"},
		{"Int16Array", "Bridge.Html5.Int16Array"},
		{"Int32Array", "Bridge.Html5.Int32Array"},
		{"Int8Array", "Bridge.Html5.Int8Array"},
		{"Prototype", "Bridge.Html5.Prototype"},
		{"Uint16Array", "Bridge.Html5.Uint16Array"},
		{"Uint32Array", "Bridge.Html5.Uint32Array"},
		{"Uint8Array", "Bridge.Html5.Uint8Array"},
		{"Uint8ClampedArray", "Bridge.Html5.Uint8ClampedArray"},
		{"ArrayLike", "ArrayList"}, {"event", "event"}, {"object", "object"}, {"params", "params" }, {"WebGLRenderingContext", "IWebGLRenderingContext" } };

		//ArrayBuffer | Int8Array | Uint8Array | Uint8ClampedArray | Int16Array | Uint16Array | Int32Array | Uint32Array | Float32Array | Float64Array | ArrayLike
		public static Dictionary<string, int> DuplicatesLeft = new Dictionary<string, int>();
		public static Dictionary<string, int> DuplicatedTypes = new Dictionary<string, int>();
		public static Dictionary<string, StringBuilder> DefinedTypes = new Dictionary<string, StringBuilder>();
		/*public string ToCsharpRepresentation(LineInfo info, out LanguageToken next)
		{
			;
			// en fait on va regarder just 
			next = this.next;
			if (scopeDescriptors.ContainsValue(this.LastLogic.tokenType))
			{
				if (DuplicatedTypes.ContainsKey(this.tokenString))
				{

					return ("");
					;
				}
			}
			if (DuplicatedTypes.ContainsKey(this.parent.scopeTokenName.tokenString))
			{
				DefinedTypes[this.parent.scopeTokenName.tokenString].Append(ToCsharpRepresentation_(info, out next));
				if (!(DuplicatesLeft.ContainsKey(this.parent.scopeTokenName.tokenString)))
				{
					DuplicatesLeft[this.parent.scopeTokenName.tokenString] = DuplicatedTypes[this.parent.scopeTokenName.tokenString];
				}
				else
				{
					DuplicatesLeft[this.parent.scopeTokenName.tokenString]--;
				}
				if (DuplicatesLeft[this.parent.scopeTokenName.tokenString] == 0)
				{
					return (DefinedTypes[this.parent.scopeTokenName.tokenString].ToString());
				}
				return ("");
			}
			return (ToCsharpRepresentation_(info, out next));
		}*/
		public static List<ScopeDescriptor> rootScopes = new List<ScopeDescriptor>();
		private static ScopeDescriptor _activeScope = null;
		public static ScopeDescriptor activeScope
		{
			get
			{
				if (_activeScope == null)
				{
					_activeScope = new ScopeDescriptor("GLOBAL", "namespaceKW");
					_activeScope.isNamespace = true;
					rootScopes.Add(_activeScope);
				}
				return (_activeScope);
			}
			set
			{
				if (_activeScope == null)
				{
					rootScopes.Add(value);
				}
				_activeScope = value;
			}
		}
		/*
		public void WorkToken(out LanguageToken next)
		{

			next = this.next;
			string trimms;
			if (this.tokenType == "semicolon" && this.GetLastLogic(out trimms, true).tokenType == "funcOUT")
			{
				next = this.next;
				return;
			}
			LanguageToken token = this;
			if (token.tokenType == "TYPE" && BuiltinTypes.ContainsKey(token.tokenString))
			{
				if (token.tokenString != "Array")
				{
					this.builtinType = BuiltinTypes[token.tokenString];
					//res.Append(this.builtinType);
				}
			}
			if (token.tokenType == "space_spacement" || token.tokenType == "tab_spacement" || token.tokenType == "newLine" || nameKinds.ContainsKey(token.tokenType) || LanguageToken.nameEnders.ContainsKey(token.tokenString))
			{

				if (nameKinds.ContainsKey(token.tokenType))
				{
					Console.WriteLine("\n\n--------------FOUND NAMEKIND?" + token.tokenType + ":" + token.tokenString);
					token.DebugALittle();
					string varName = token.tokenString;
					if (BuiltinTypes.ContainsValue(varName))
					{
						varName = "[Name(\"" + varName + "\")]\n@" + varName;
					}
					switch (token.tokenType)
					{
						case "methodName":
							Console.WriteLine(this.tokenType);
							MethodDescriptor method = this.BuildMethod(varName, out next, false);
							activeScope.AddMethod(method);
							return;
						case "memberName":
							Console.WriteLine("BUILDING MEMBER");
							MemberDescriptor member = this.BuildMember(varName, out next);
							Console.WriteLine("MEMBER BUILT, NEXT:");
							next.DebugALittle();
							activeScope.SetMember(member);
							return;
						case "varName":
						case "constantVar":
							ValueDescriptor value = BuildValue(varName, out next);
							activeScope.SetValue(value);
							return;
						case "functionName":
							FunctionDescriptor function = BuildFunction(varName, out next);
							activeScope.AddFunction(function);
							return;

						case "interfaceName":
						case "enumName":
						case "className":
							ScopeDescriptor scope = new ScopeDescriptor(varName, token.LastLogic.tokenType);
							if (token.tokenType == "enumName")
							{
								scope.isEnum = true;
							}
							if (token.tokenType == "namespaceName")
							{

								scope.isNamespace = true;
							}
							if (activeScope != null)
							{

								activeScope.scopeChildrens.Add(varName, scope);
								scope.parentScope = activeScope;
							}
							List<string> implementTypes = new List<string>();
							List<string> extendTypes = new List<string>();
							List<GenericTypeParameterDescriptor> genericParams = new List<GenericTypeParameterDescriptor>();
							LanguageToken current = token.NextLogic;
							Console.WriteLine("FOUND CLASSNAME...");

							if (current.tokenType == "typeIN")
							{
								Console.WriteLine("found type in");
								current = current.NextLogic;
								while (current.tokenType == "TYPE")
								{
									string extends = "";
									string name = current.tokenString;
									Console.WriteLine("name:" + name);
									if (current.NextLogic.tokenType == "extendsKW")
									{
										extends = current.NextLogic.NextLogic.virtualTypeName(null, out current);
									}
									else
									{
										current = current.NextLogic;
									}
									genericParams.Add(new GenericTypeParameterDescriptor(name, extends));
									if (current.tokenType != "comma")
									{
										current = current.NextLogic;
										break;
									}
									else
									{
										Console.WriteLine("was comma");
										current = current.NextLogic;
									}
								}
							}
							else
							{
								Console.WriteLine("not typeIN:" + current.tokenType);
							}
							if (current.tokenType == "extendsKW")
							{
								current = current.NextLogic;
								Console.WriteLine("EXTENDS...!");
								while (current.tokenType == "TYPE" || current.tokenType == "genericTYPE")
								{
									extendTypes.Add(current.virtualTypeName(null, out current));
									if (current.tokenType != "comma")
									{
										current = current.NextLogic;
										break;
									}
									else
									{
										current = current.NextLogic;
									}
								}
							}
							else
							{
								current.DebugALittle();
							}
							if (current.tokenType == "implementsKW")
							{
								current = current.NextLogic;
								while (current.tokenType == "TYPE")
								{
									implementTypes.Add(current.tokenString);
									if (current.NextLogic.tokenType != "comma")
									{
										current = current.NextLogic;
										break;
									}
									else
									{
										current = current.NextLogic.NextLogic;
									}
								}

								;
							}
							else
							{
								current.DebugALittle();
							}
							scope.genericParameters = genericParams;
							scope.tmpExtends = extendTypes;
							scope.tmpImplements = implementTypes;
							activeScope = scope;
							return;
						default:
							break;
					}
				}
				next = token.next;
				return;
			}
			else
			{

				if (token.tokenType == "constructorKW")
				{
					Console.WriteLine("---------------FOUND CONSTRUCTOR");
					//throw new System.Exception("ok");
					LanguageToken name = this.parent.scopeToken.NextLogic;
					ConstructorDescriptor constructor = this.BuildConstructor(out next);
					activeScope.AddConstructor(constructor);
					return;
				}
				if (token.tokenType == "namespaceKW")
				{
					LanguageToken name = this.NextLogic;
					bool duplicate = false;

					if (activeScope != null && (activeScope.namespaceChildrens.ContainsKey(name.tokenString) || activeScope.scopeChildrens.ContainsKey(name.tokenString)))
					{
						duplicate = true;
					}
					else
					{
						duplicate = false;
					}
					ScopeDescriptor scope = new ScopeDescriptor(name.tokenString, "namespaceKW", duplicate);
					if (activeScope != null)
					{
						activeScope.namespaceChildrens.Add(name.tokenString, scope);
						scope.parentScope = activeScope;
					}
					activeScope = scope;
					scope.isNamespace = true;
					// pas finit faut mettre en tant que nom actif ou un truc du genre.
					next = name.next;
					return;
				}
				else if (token.tokenType == "interfaceKW")
				{
					//res.Append("public class");
					next = token.next;
					return;// (res.ToString());
					;
				}
				else if (token.tokenType == "extendsKW")
				{
					//res.Append(":");
					next = token.next;
					return;// (res.ToString());
				}
				else if (token.tokenType == "implementsKW")
				{
					while (next.tokenType != "scopeIN")
					{
						next = next.NextLogic;
					}
					return;
					//return ("");
				}
				else if (token.tokenType == "functionKW")
				{
					next = token.next;
					return;// ("");
				}
				else if (token.tokenType == "letKW" || token.tokenType == "varKW")
				{
					next = token.next;
					return;// ("");
				}
				else if (token.tokenType == "funcIN")
				{
					LanguageToken lastLogic = null;
					string trims = null;
					switch (token.funcType)
					{
						case FuncType.LambdaFunction:
							trims = null;
							lastLogic = this.GetLastLogic(out trims, true);
							string tmp = this.CSharpLambdaTraduction(null, out next);
							throw new System.Exception("wrong lambda");
							break;
						case FuncType.Func:
							trims = null;
							lastLogic = this.GetLastLogic(out trims, true);
							FunctionDescriptor function = BuildFunction(lastLogic.tokenString, out next, false);
							break;
							/*case FuncType.TypeExpression:
								res.Append(this.CSharpTypeExpression(info, out next));
								break;
							case FuncType.Func:
								res.Append(this.CSharpFunctionDefinition(info, out next));
								break;*/
								/*

					}
					return;// (res.ToString());
						   //res.Append(token.funcType.ToString() + "IN");

					;
				}
				else if (token.tokenType == "constKW")
				{

					if (this.NextLogic.NextLogic.tokenType == "colons" && registeredEnums.ContainsKey(this.NextLogic.NextLogic.NextLogic.tokenString))
					{
						registeredEnums[this.NextLogic.NextLogic.NextLogic.tokenString].AddValue(this.NextLogic, out next);
						return;
					}
					else if (this.NextLogic.tokenType == "constantVar")
					{
						return;
					}
				}
				else if (token.tokenType == "enumKW")
				{
					next = token.next;
					//CSharpEnumDefinition.FromToken(token, out next);
					return;
				}
				else if (token.tokenType == "funcOUT")
				{
					;//throw new System.Exception("missing '(' at begining");
				}
				else if (token.tokenType == "publicKW")
				{
					//res.Append("");
					next = token.next;
					return; //(res.ToString());
				}
				else if (token.tokenType == "exportKW")
				{
					next = token.next;
					return;// ("");
				}
				else if (token.tokenType == "declareKW")
				{
					next = token.next;
					return;// ("");
				}
				else if (token.tokenType == "staticKW")
				{
					next = token.next;
					return;// ("");
				}
				else if (token.tokenType == "classKW")
				{
					next = token.next;

					return;// ("public class");
				}
				else if (token.tokenType == "scopeIN")
				{
					switch (token.scopeType)
					{
						case ScopeType.ObjectDescriptor:
							string type = this.virtualTypeName(null, out next);
							return;
						case ScopeType.Scope:
							ActiveScope = this.scopeToken;
							next = token.next;
							return;
							;
					}
				}
				else if (token.tokenType == "scopeOUT")
				{

					ActiveScope = token.parent;
					if (token.parent != null)
					{
						if (token.parent.scopeToken.tokenType == "namespaceKW")
						{
							//List<string> toRemove = new List<string>();
							foreach (string enumName in LanguageToken.registeredEnums.Keys)
							{
								//res.Append("\n");
								//res.Append(LanguageToken.registeredEnums[enumName].stringObject);
							}
							LanguageToken.registeredEnums = new Dictionary<string, CSharpEnumDefinition>();

							;
							;
						}
						Console.WriteLine("----------------------END OF SCOPE----------------------");
						if (token.parent.scopeToken.tokenType == "classKW" || token.parent.scopeToken.tokenType == "namespaceKW" || token.parent.scopeToken.tokenType == "enumKW")
						{

							activeScope = activeScope.parentScope;
						}
						else
						{
							Console.WriteLine("WEIRD PARENT:");
							token.parent.scopeToken.DebugALittle();
						}
					}
					//res.Append("}");
					return;
				}
				else if (token.tokenType == "genericTYPE")
				{

					//res.Append("<<<" + token.tokenString + "::::>");
					token = token.NextLogic;
					if (token.parameters.Count > 0)
					{
						LanguageToken nextOfParam = null;
						for (int i = 0; i < token.parameters.Count; i++)
						{


							token.parameters[i].ToStringRepresentation(null, out nextOfParam);
							if (i + 1 < token.parameters.Count)
							{
								//res.Append(",");
							}
						}
						next = nextOfParam;
					}
					//res.Append(">>>");
				}
				else
				{
					//res.Append(token.tokenString);
					next = token.next;
					return;//return (res.ToString());
				}

				;
			}
		}*/
		public LanguageToken LastNameKind
		{
			get
			{
				LanguageToken tmp = this;
				while (!nameKinds.ContainsKey(tmp.tokenType))
				{
					tmp = tmp.LastLogic;
				}
				return (tmp);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="next"></param>
		/// <returns></returns>
		public string ToStringRepresentation(LineInfo info, out LanguageToken next, bool logicOnly = true)
		{
			next = this.next;
			StringBuilder res = new StringBuilder("");
			LanguageToken token = this;
				
				
				/*
				"commentedLine";
		}
			if (isCommentedBlock)
			{
				tokenType = "commentedBlock";*/ 
			if (token.tokenType == "space_spacement" || token.tokenType == "tab_spacement" || token.tokenType == "newLine")
			{
				res.Append(token.tokenString);
			}
			else
			{
				if (token.tokenType == "funcIN")
				{
					res.Append(token.funcType.ToString() + "IN");
					;
				}
				else if (token.tokenType == "funcOUT")
				{
					res.Append(token.funcType.ToString() + "OUT");
					;
				}
				else if (token.tokenType == "scopeIN")
				{
					res.Append(token.scopeType.ToString() + "IN");
					;
				}
				else if (token.tokenType == "scopeOUT")
				{
					res.Append(token.scopeType.ToString() + "OUT");
					;
				}
				else
					res.Append(token.tokenType);
				if (LanguageToken.nameKinds.ContainsKey(token.tokenType) || token.tokenType == "TYPE" || token.tokenType == "NAME" || token.tokenType == "VALUE")
				{
					res.Append("<<<" + token.tokenString);
					while (token.NextLogic.tokenType == "arrayIN" && token.NextLogic.NextLogic.tokenType == "arrayOUT")
					{
						res.Append("[]");
						token = token.NextLogic.NextLogic;
					}
					res.Append(">>>");

				}
				else if (token.tokenType == "genericTYPE")
				{
					res.Append("<<<" + token.tokenString + "::::>");
					token = token.NextLogic;
					if (token.parameters != null && token.parameters.Count > 0)
					{
						LanguageToken nextOfParam = null;
						for (int i = 0; i < token.parameters.Count; i++)
						{


							res.Append(token.parameters[i].ToStringRepresentation(info, out nextOfParam));
							if (i + 1 < token.parameters.Count)
							{
								res.Append(",");
							}
						}
						token = nextOfParam;
					}
					res.Append(">>>");
				}
			}
			next = token.next;
			return (res.ToString());
			;
		}
		public ScopeDescriptor descriptor = null;
		public LanguageToken lambdaReturnShortcut = null;
		public List<GenericTypeParameterDescriptor> BuildGenericParameters(out LanguageToken next)
		{
			List<GenericTypeParameterDescriptor> res = new List<GenericTypeParameterDescriptor>();
			next = this.NextLogic;
			if (next.tokenType == "typeIN")
			{
				//throw new System.Exception("ok");
				//Console.WriteLine("FOUND TYPE IN");
				next = next.NextLogic;
				while (true)
				{
					if (next.tokenType == "TYPE")
					{
						//throw new Exception("ok2");
						res.Add(new GenericTypeParameterDescriptor(next.tokenString));
						next = next.NextLogic;
						if (next.tokenType == "extendsKW")
						{
							res[res.Count - 1].extends = next.NextLogic.virtualTypeName2(null, out next);
						}
						if (next.tokenType == "comma")
						{
							next = next.NextLogic;
						}
						else
						{
							next = next.NextLogic;
							break;
						}
					}
					else
						break;
				}
			}
			return (res);
		}
		// extends on va tout lire les extends d'un coup.
		// lambda on va lire d'un coup.
		// classKW on va regarder tout ce qui vient juste avant le scopeIN
		// enumKW pareil
		// namespaceKW pareil
		// on vient dire que c'est l'activeScope.
		// type générique il va falloir le faire aussi donc ce serait dommage, on va voir au lexer pour le mettre en mode "TYPE" NON: on lit tout dans le sens qui vient. tranquil.
		//public static string activeType = "";
		public void WorkToken2(out LanguageToken next)
		{
			TypeDescriptor typeName = null;
			string varName = "";
			string varValue = "";
			next = this.next;
			if (this.tokenType == "scopeOUT")
			{
				if (activeScope != null)
				{
					activeScope = activeScope.parentScope;
				}
			}
			if (this.tokenType == "enumKW")
			{
				varName = this.NextLogic.tokenString;
				ScopeDescriptor scope = new ScopeDescriptor(this.NextLogic.tokenString, this.tokenType);
				if (activeScope != null)
				{
					Console.WriteLine("VARNAME:" + varName);
					activeScope.scopeChildrens.Add(varName, scope);
					scope.parentScope = activeScope;
				}
				scope.isEnum = true;
				activeScope = scope;
				next = this.NextLogic.NextLogic;
			}
			#region class
			if (this.tokenType == "classKW" || this.tokenType == "namespaceKW" || this.tokenType == "interfaceKW" || this.tokenType == "moduleKW")
			{
				
				Console.WriteLine("Class!");
				ScopeDescriptor scope = new ScopeDescriptor(this.NextLogic.tokenString, this.tokenType);
				if (this.tokenType == "moduleKW")
				{
					scope.isModule = true;
				}
				if (this.tokenType == "namespaceKW") //&& (activeScope == null || activeScope.isNamespace == true)) // DUPLICAT
				{
					scope.isNamespace = true;
				}
				else
					scope.isNamespace = false;
				
				varName = this.NextLogic.tokenString;
				if (this.NextLogic.tokenType == "textLittle" || this.NextLogic.tokenType == "textBIG")
				{
					//throw new System.Exception("ok");
					varName = this.NextLogic.NextLogic.tokenString;
					scope.scopeName = varName;
					LanguageToken current = this.NextLogic.NextLogic;
					StringBuilder moduleName = new StringBuilder("");
					while (current.tokenType != "textLittle" && current.tokenType != "textBIG")
					{
						moduleName.Append(current.tokenString);
						current = current.NextLogic;

					}
					scope.moduleName = moduleName.ToString();

					//throw new System.Exception("varName:" + varName);
				}
				if (activeScope != null)
				{
					if (scope.isNamespace)
					{
						activeScope.namespaceChildrens.Add(varName, scope);
					}
					else
					{
						
						string trueVarName = varName;
						/*if (activeScope.scopeChildrens.ContainsKey(trueVarName)) // DUPLICAT
						{
							for (int i = 0; i >= 0 && activeScope.scopeChildrens.ContainsKey(trueVarName); i++)
							{
								trueVarName = varName + "_";
								if (i > 0)
								{
									trueVarName = varName + "_" + i;
								}
							}
							scope.duplicate = true;
							scope.duplicateName = trueVarName;
						}*/
						activeScope.scopeChildrens.Add(trueVarName, scope);
					}
					
					scope.parentScope = activeScope;
				}
				List<GenericTypeParameterDescriptor> genericParams = new List<GenericTypeParameterDescriptor>();
				List<TypeDescriptor> extendTypes = new List<TypeDescriptor>();
				List<TypeDescriptor> implementTypes = new List<TypeDescriptor>();
				next = this.NextLogic;
				scope.genericParameters = next.BuildGenericParameters(out next);
				/*if (next.tokenType == "typeIN")
				{
					//Console.WriteLine("FOUND TYPE IN");
					next = next.NextLogic;
					while (true)
					{
						if (next.tokenType == "TYPE")
						{
							genericParams.Add(new GenericTypeParameterDescriptor(next.tokenString));
							next = next.NextLogic;
							if (next.tokenType == "extendsKW")
							{
								genericParams[genericParams.Count - 1].extends = next.NextLogic.virtualTypeName(null, out next);
							}
							if (next.tokenType == "comma")
							{
								next = next.NextLogic;
							}
							else
							{
								next = next.NextLogic;
								break;
							}
						}
						else
							break;
					}
				}*/
				//Console.WriteLine("NEXT:");
				//next.DebugALittle();
				//Console.WriteLine("end");
				if (next.tokenType == "extendsKW")
				{
					next = next.NextLogic;
					Console.WriteLine("EXTENDS...!");
					while (next.tokenType == "TYPE" || next.tokenType == "genericTYPE")
					{
						extendTypes.Add(next.virtualTypeName2(null, out next));
						if (next.tokenType != "comma")
						{
							//next = next.NextLogic;
							break;
						}
						else
						{
							next = next.NextLogic;
						}
					}
				}

				if (next.tokenType == "implementsKW")
				{
					//Console.WriteLine("IMPLEMENTS...!");
					next = next.NextLogic;
					while (next.tokenType == "TYPE" || next.tokenType == "genericTYPE")
					{
						implementTypes.Add(next.virtualTypeName2(null, out next));
						if (next.NextLogic.tokenType != "comma")
						{

							break;
						}
						else
						{
							next = next.NextLogic.NextLogic;
						}
					}

				}
				else
				{
					//Console.WriteLine("CANT FIND IMPLEMENTS?:");
					//next.DebugALittle();
				}
				//Console.WriteLine("NEXT:" + next.tokenType);
				//Console.WriteLine("DONE ADD SCOPE");
			
				scope.extends = extendTypes;
				scope.implements = implementTypes;
				activeScope = scope;
			}
			#endregion
			bool isOptionnal = false;
			bool renamed = false;
			string formatedVarName = "";
			bool isStatic = (this.LastLogic != null && this.LastLogic.tokenType == "staticKW");
			if (this.tokenType == "constructorKW")
			{
				Console.WriteLine("building constructor");
				activeScope.AddConstructor(BuildConstructor(out next));
				Console.WriteLine("===========================AFTER CONSTRUCTOR:");
				next.DebugALittle();
			}
			else if (this.tokenType == "NAME")
			{
				;
				
				if (this.LastLogic != null)
				{
					typeName = null;
					varName = this.tokenString;
					if (varName != this.formatedVarName)
					{
						renamed = true;
						formatedVarName = this.formatedVarName;
					}
					//Console.WriteLine("???");
					string nameKind = "";
					Console.WriteLine("FOUND NAME:" + varName);
					LanguageToken current = this;
					if (current.NextLogic.tokenType == "optionnalParameter")
					{
						current = current.NextLogic;
					}
					if (current.NextLogic.tokenType == "colons")
					{
						typeName = current.NextLogic.virtualTypeName2(null, out next);
						if (next.tokenType == "equal")
						{
							varValue = next.NextLogic.tokenString;
						}
					}
					else if (current.NextLogic.tokenType == "funcIN")
					{
						Console.WriteLine("ITS A METHOD");
						//MethodDescriptor desc = new MethodDescriptor(varName, "", current.NextLogic, current.NextLogic.parent);
						MethodDescriptor method = BuildMethod(varName, out next);
						activeScope.AddMethod(method);
						method.isStatic = isStatic;
					}
					else if (current.NextLogic.tokenType == "typeIN")
					{
						List<GenericTypeParameterDescriptor> genericParameters = current.BuildGenericParameters(out next);
						if (next.tokenType == "funcIN")
						{
							MethodDescriptor method = next.LastLogic.BuildMethod(varName, out next);
							method.isStatic = isStatic;
							activeScope.AddMethod(method);
							method.genericParameters = genericParameters;
						}



					}
					else if (current.NextLogic.tokenType == "semicolon" || ((current.NextLogic.tokenType == "comma" || current.NextLogic.tokenType == "scopeOUT") && activeScope != null && activeScope.scopeType == "enumKW"))
					{

						typeName = new TypeDescriptor("object");
					}
					//
				}
			}
			else if (this.tokenType == "TYPE" || this.tokenType == "genericTYPE")
			{
				Console.WriteLine("???2");
				typeName = this.virtualTypeName2(null, out next);
				if (next.tokenType == "NAME")
				{
					varName = next.tokenString;
					next = next.NextLogic;
				}
			}
			else
			{
				
				next = this.NextLogic;
			}
			if (typeName != null && varName != "")
			{

				Console.WriteLine("FOUND TYPENAME:" + typeName + ", FOR NAME:" + varName + ";" + (varValue != "" ? varValue : ""));
				if (activeScope != null)
				{
					MemberDescriptor member = new MemberDescriptor(varName, typeName, null, null, renamed, formatedVarName);
					member.isStatic = isStatic;
					activeScope.SetMember(member);
				}
			}
		}

		public bool inTypeList = false;
		public LanguageToken(string tokenString, int startPosition = -1, int endPosition = -1, int lineNumber = -1, StringBuilder textFile = null, string tokenType = "", LanguageToken last = null)
		{
			//Console.WriteLine("robo");
			// dans le constructeur on va vérif qu'on a besoin de mettre le précédent comme parent ou pas,
			// toute histoire de correspondance se gère avec depuis le constructeur du suivant. 
			Console.WriteLine("LANGUAGE TOKEN:>##" + tokenString + "##<");
			Console.WriteLine("TYPE:" + tokenType);
			this.last = last;
			if (this.last != null)
				this.lineBegining = this.last.lineBegining;
			
	

			// 04 // POUR LES DECL, SOIT ON A CROISE VAR AVANT, SOIT ON A CROISE

			this.tokenString = tokenString;
			this.tokenType = tokenType;
			this.startPosition = startPosition;
			this.endPosition = endPosition;
			this.textFile = textFile;
			this.lineNumber = lineNumber;
			if (this.LastLogic != null)
			{
				if (this.LastLogic.tokenType == "typeIN")
				{
					this.parent = this.LastLogic;
				}
				else
					this.parent = this.LastLogic.parent;
				if (this.LastLogic.tokenType == "typeOUT")
				{
					this.parent = this.LastLogic.parent.parent;
				}
			}
			

			if (this.tokenType == "")
			{
				
				this.tokenType = "NAME";
				if (this.LastLogic != null)
				{
					if (this.LastLogic.tokenType == "extendsKW" || this.LastLogic.tokenType == "implementsKW" || this.LastLogic.tokenType == "comma" && this.LastLogic.LastLogic.inTypeList)
					{
						this.inTypeList = true;
						this.tokenType = "TYPE";
					}
				}
				Console.WriteLine("ITS A NAME?");
				if (this.LastLogic.tokenType == "equal")
				{
					this.tokenType = "VALUE";
				}
				if (this.parent != null && this.parent.tokenType == "typeIN")
				{
					this.tokenType = "TYPE";
				}
				else if (this.LastLogic != null)
				{
					if (this.LastLogic.tokenType == "NAME")
					{
						this.LastLogic.tokenType = "TYPE";
						this.tokenType = "NAME";
					}
					else if (this.LastLogic.tokenType == "or")
					{
						this.tokenType = "TYPE";
					}
					else if (this.LastLogic.tokenType == "dot")
					{
						LanguageToken current = this.LastLogic.LastLogic;
						this.tokenType = current.tokenType;
			
						//} 
						//this.tokenType = "TYPE";
					}
					else if (this.LastLogic.tokenType == "colons")
					{
						this.tokenType = "TYPE";
					}
				}
			}
			if (this.LastLogic != null)
			{
				if (this.LastLogic.tokenType == "TYPE")
				{
					if (this.tokenType == "typeIN")
					{ 
						this.LastLogic.tokenType = "genericTYPE";
					}
				}
			}
			

			//Console.WriteLine("yoyo");
			if (this.tokenString == "\n")
			{
				Console.WriteLine("line:" + this.lineNumber);
			}
			if (!(this.tokenString != "" && (this.tokenType == "" || this.tokenType == "tab_spacement" || this.tokenType == "space_spacement")))
			{
				if (!(occurences.ContainsKey(this.tokenString)))
				{
					occurences.Add(this.tokenString, new List<LanguageToken>());
				}
				if (this.tokenType != "")
				{
					if (!(typeOccurences.ContainsKey(this.tokenType)))
					{
						typeOccurences.Add(this.tokenType, new List<LanguageToken>());
					}
					typeOccurences[this.tokenType].Add(this);
				}
				occurences[this.tokenString].Add(this);
			}
			//Console.WriteLine("toto");

		}
		public bool tokenEnder = false;
		public LanguageToken lineBegining = null;
		public LanguageToken lineEnding = null;
		public LanguageToken next = null;
		public void AddParameter(LanguageToken toAdd)
		{
			this.parameters.Add(toAdd);
		}
		public LanguageToken last = null; // prochain effecteur. ( "maurice", ".", "add(","machintruc", ","
		public LanguageToken parent = null; // container parent.
		public bool isContainer = false; // functionParameters (avant ça il y a un token fonction) , scope (avant ça il y a par exemple, un token class/ ou un token fonctionparameters)  pour   function/class/namespace/enum/interface
		public bool isCommented = false;
		public List<LanguageToken> childrens = new List<LanguageToken>();

		public bool isTokenEnder
		{
			get
			{
				return (tokenEnders.ContainsValue(this.tokenType));
			}
		}
		public bool findCustom(int index, string customString)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = index; i < index + customString.Length && i < this.textFile.Length; i++)
			{
				if (this.textFile[i] != customString[i - index])
				{
					return (false);
				}
			}
			return (true);
		}
		public bool findTokenEnder(int index, int minTokenLen, int maxTokenLen)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = index; i < index + maxTokenLen && i < maxTokenLen + index && i < this.textFile.Length; i++)
			{
				if (tokenEnders.ContainsKey(sb.ToString()))
				{
					//Console.WriteLine("TOKEN ENDER:" + sb.ToString());
					return (true);
				}
				sb.Append(this.textFile[i]);
			}
			return (false);
		}

		public bool isSpacement = false;

		public LanguageToken ReadNext(bool andContinue = false)
		{
			int keyLen = 0;
			//Console.WriteLine("NEXT");
			bool keywordToken = false;
			bool tokenEnder = false;
			bool newLine = false;
			bool scopeDelimiter = true;
			char spacementType = default(char);
			string predictedType = "";
			bool spacement = false;
			bool isContainerBeginSpecifier = false;
			bool isContainerEndSpecifier = false;
			bool isCommentedLine = false;
			bool isCommentedBlock = false;
			int containerIndex = 0;
			StringBuilder sb = new StringBuilder();
			int minKeyWordLen = -1;
			int maxKeyWordLen = -1;
			int minTokenEnderLen = -1;
			int maxTokenEnderLen = -1;
			foreach (string key in keywordTokens.Keys)
			{
				int len = key.Length;
				maxKeyWordLen = (maxKeyWordLen < len || maxKeyWordLen == -1) ? len : maxKeyWordLen;
				minKeyWordLen = (minKeyWordLen > len || minKeyWordLen == -1) ? len : minKeyWordLen;
			}
			foreach (string key in tokenEnders.Keys)
			{
				int len = key.Length;
				maxTokenEnderLen = (maxTokenEnderLen < len || maxTokenEnderLen == -1) ? len : maxKeyWordLen;
				minTokenEnderLen = (minTokenEnderLen > len || minTokenEnderLen == -1) ? len : minTokenEnderLen;
			}
			bool confirmedKeyword = false;
			//Console.WriteLine("MAX KEYWORD LEN:" + maxKeyWordLen);
			//Console.WriteLine("MIN KEYWORD LEN:" + minKeyWordLen);
			//Console.WriteLine("MAX TOKENENDER LEN:" + maxTokenEnderLen);
			//Console.WriteLine("MIN TOKENENDER LEN:" + minTokenEnderLen);
			for (int i = this.endPosition; i < this.textFile.Length; i++)
			{

				if ((this.textFile[i] == ' ' || this.textFile[i] == '\t'))
				{
					if (i == this.endPosition)
					{
						spacement = true;
						spacementType = this.textFile[i];
					}
					else if (!spacement || spacementType != this.textFile[i])
					{
						confirmedKeyword = true;
						break;
					}
				}
				else if (spacement)
					break;
				if (this.textFile[i] == '\n')
				{
					confirmedKeyword = true;
					if (i == this.endPosition)
					{
						sb.Append(this.textFile[i]);
						newLine = true;
						break;
					}
					else
					{

						break;
						;
					}
				}

				// non d'ici on doit aussi vérifier que c'est pas un nouveau delimiter/ender utilisé d'un coup.
				//int j = 0;
				if (i != this.endPosition && !findTokenEnder(this.endPosition, minTokenEnderLen, maxTokenEnderLen) && findTokenEnder(i, minTokenEnderLen, maxTokenEnderLen))
				{
					confirmedKeyword = true;
					break;
				}
				else if (findCustom(i, "//") || findCustom(i, "#"))
				{
					confirmedKeyword = true;
					sb.Append("//");
					isCommentedLine = true;
					for (int j = i + "//".Length; j < this.textFile.Length; j++)
					{
						if (this.textFile[j] == '\n')
						{

							break;
						}
						sb.Append(this.textFile[j]);
					}
					break;

					;
				}
				else if (findCustom(i, "/*"))
				{
					confirmedKeyword = true;
					sb.Append("/**");
					isCommentedBlock = true;
					for (int j = i + "/*".Length; j < this.textFile.Length; j++)
					{
						if (findCustom(j, "*/"))
						{
							sb.Append("*/");
							break;
						}
						sb.Append(this.textFile[j]);
					}
					break;
					;
				}
				sb.Append(this.textFile[i]);

				if (i - this.endPosition >= minKeyWordLen - 1 && i - this.endPosition <= maxKeyWordLen)
				{
					;
					if (keywordTokens.ContainsKey(sb.ToString()))
					{
						keyLen = i - this.endPosition + 1;
						//Console.WriteLine("KEYWORd FUCKIN FOUND!!!!!!!:" + sb.ToString());
						keywordToken = true;
						//break;
					}
					else
					{
						keywordToken = false;
					}
				}
				else
				{
					keywordToken = false;
				}
				if (i - this.endPosition >= minTokenEnderLen - 1 && i - this.endPosition <= maxTokenEnderLen)
				{
					if (tokenEnders.ContainsKey(sb.ToString()))
					{
						keyLen = i - this.endPosition + 1;

						//Console.WriteLine("TOKEN ENDER FOUND" + sb.ToString());
						tokenEnder = true;
					}
				}
			}
			string text = tokenEnder ? (sb.Length > keyLen ? sb.Remove(keyLen, sb.Length - keyLen).ToString() : sb.ToString()) : sb.ToString();
			//Console.WriteLine("KEYWORD:" + keywordToken);
			//Console.WriteLine("KEY LEN:" + keyLen);
			//Console.WriteLine("ENDER:" + tokenEnder + ":" + text);
			string tokenType = (keywordToken && confirmedKeyword) ? keywordTokens[text] : (tokenEnder ? tokenEnders[text] : "");
			if (isCommentedLine)
			{
				tokenType = "commentedLine";
			}
			if (isCommentedBlock)
			{
				tokenType = "commentedBlock";
				//Console.WriteLine("COMMENTED BLOCK:" + sb.ToString());
			}
			if (spacement)
			{
				tokenType = spacementType == ' ' ? "space_spacement" : "tab_spacement";
			}
			if (newLine)
			{
				tokenType = "newLine";
			}
			//Console.WriteLine("FOUND TYPE:" + tokenType);
			LanguageToken next = new LanguageToken(text, endPosition, endPosition + text.Length, this.tokenString == "\n" ? this.lineNumber + 1 : this.lineNumber, this.textFile, tokenType, this);
			this.next = next;

			return (next);
			//
			/*bool begin = false;
            bool end = false;
            bool check = false;
            for (int j = 0; j < ContainerBeginSpecifiers.Count; j++)
            {
                begin = true;
                check = false;
                if (ContainerBeginSpecifiers[j][0] == this.textFile[i])
                {
                    check = true;
                    for (int k = 0; k < ContainerBeginSpecifiers[j].Length; k++)
                    {
                        if (this.textFile[i + k] != ContainerBeginSpecifiers[j][k])
                        {
                            check = false;
                            break;
                        }
                    }
                }
                check = false;
                if (check)
                {
                    isContainerBeginSpecifier = true;
                    containerIndex = j;
                    break;
                }
                begin = false;
                end = true;
                check = false;
                if (ContainerEndSpecifiers[j][0] == this.textFile[i])
                {
                    check = true;
                    for (int k = 0; k < ContainerEndSpecifiers[j].Length; k++)
                    {
                        if (this.textFile[i + k] != ContainerEndSpecifiers[j][k])
                        {
                            check = false;
                            break;
                        }
                    }
                }
                check = false;
                if (check)
                {
                    isContainerBeginSpecifier = true;
                    containerIndex = j;
                    break;
                }
                end = false;
                ;
            }
            if (check)
            {
                if (i == startPosition)
                {
                    break;
                }
                else
                {
                    break;
                }
            }
        }*/


		}
	}

	internal class Program
	{
		private static void Main(string[] args)
		{
			for (int i = 0; i < args.Length; i++)
			{
				Console.WriteLine(args[i]);
			}
			if (args.Length > 0)
			{
				foreach (var key in LanguageToken.scopeDescriptors.Keys)
				{
					LanguageToken.nameKinds[key + "Name"] = true;
				}
				string filename = args[0];
				string file = System.IO.File.ReadAllText(args[0]);
				file += "\n";
				StringBuilder sb = new StringBuilder(file);
				StringBuilder formatted = new StringBuilder();
				for (int i = 0; i < sb.Length; i++)
				{
					if (sb[i] != (char)13)
					{
						formatted.Append(sb[i]);
					}

				}
				sb = formatted;
				//Console.WriteLine("TEXT:" + sb.ToString());
				LanguageToken token = new LanguageToken("", 0, 0, 0, sb, "FileBegining");
				LanguageToken current = token;
				while ((current = current.ReadNext(true)) != null)
				{
					if (current.endPosition >= current.textFile.Length - 1)
						break;
				}
				StringBuilder parsedFile = new StringBuilder();
				//token.ReadNext(true);
				parsedFile.Append("using System;\n");
				LanguageToken token2 = token;

				while (token != null)
				{
					LanguageToken last = token;
					LanguageToken.readyToBuild = true;
					LineInfo lineInfo = new LineInfo();
					//string str = token.ToCsharpRepresentation(lineInfo, out token);
					token.WorkToken2(out token);
					if (last == token)
					{
						throw new System.Exception("same token loop");
					}
					//while (
					//string str = token.ToCsharpRepresentation(new LineInfo(), out token);
					//Console.Write(str);
					//parsedFile.Append(str);
				}

				while (token2 != null)
				{

					Console.WriteLine(token2.ToStringRepresentation(null, out token2, false));
				}
				
				Console.WriteLine("RESULT:" + "\n");
				for (int i = 0; i < LanguageToken.rootScopes.Count; i++)
				{
					LanguageToken.rootScopes[i].WorkDuplicates();
					//Console.WriteLine("ROOT SCOPE:" + LanguageToken.rootScopes[i].scopeName + ";");

				}
				StringBuilder resFile = new StringBuilder();
				resFile.Append(LanguageToken.FileBegining);
				for (int i = 0; i < LanguageToken.rootScopes.Count; i++)
				{
					LanguageToken.rootScopes[i].CheckTypes();
					resFile.Append(LanguageToken.rootScopes[i].ToString());
					Console.WriteLine(LanguageToken.rootScopes[i].ToString());
				}


				if (args.Length > 1)
				{
					string outputFileName = args[1];
					
					System.IO.File.WriteAllText(args[1], resFile.ToString());
				}
			}
		}
	}
}