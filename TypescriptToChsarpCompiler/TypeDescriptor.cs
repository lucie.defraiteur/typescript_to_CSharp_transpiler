﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace TypescriptToCSharpTranspiler
{
	namespace Reflection
	{
		public class ValueDescriptor
		{
			public string rename = "";
			public string name;
			public TypeDescriptor type = null;
			public bool isConstant = false;
			public ValueDescriptor(string name, TypeDescriptor type)
			{
				this.name = name;
				this.type = type;
			}
		}
		public class FunctionDescriptor : ValueDescriptor
		{
			public List<GenericTypeParameterDescriptor> genericParameters = new List<GenericTypeParameterDescriptor>();
			public List<ParameterDescriptor> parameters = new List<ParameterDescriptor>();
			public FunctionDescriptor(string name, TypeDescriptor type, LanguageToken token, LanguageToken parent) : base(name, type)
			{
			}
			public string ToString(int lvl = 0)
			{
				List<GenericTypeParameterDescriptor> constrainteds = new List<GenericTypeParameterDescriptor>();
				StringBuilder res = new StringBuilder();
				for (int i = 0; i < lvl; i++)
				{
					res.Append("\t");
				}
				res.Append("public ");
				res.Append("static ");
				res.Append(this.type);
				res.Append(" ");
				res.Append(this.name);
				if (this.genericParameters.Count > 0)
				{
					res.Append("<");
					for (int i = 0; i < this.genericParameters.Count; i++)
					{
						res.Append(this.genericParameters[i].name);
						if (this.genericParameters[i].extends != null)
						{
							constrainteds.Add(this.genericParameters[i]);
						}
						if (i + 1 < this.genericParameters.Count)
						{
							res.Append(", ");
						}
					}
					res.Append(">");
				}
				res.Append("(");
				for (int i = 0; i < this.parameters.Count; i++)
				{
					res.Append(this.parameters[i].ToString());
					if (i + 1 < this.parameters.Count)
					{
						res.Append(", ");
					}
				}
				res.Append(")");
				if (constrainteds.Count > 0)
				{

					for (int i = 0; i < constrainteds.Count; i++)
					{
						res.Append(" where ");
						res.Append(constrainteds[i].name);
						res.Append(" : ");
						res.Append(constrainteds[i].extends);
						if (i + 1 < constrainteds.Count)
							res.Append(" ");
					}
				}
				res.Append("\n");
				for (int i = 0; i < lvl; i++)
				{
					res.Append("\t");
				}
				res.Append("{\n");
				for (int i = 0; i < lvl + 1; i++)
				{
					res.Append("\t");
				}

				res.Append("throw new System.NotImplementedException();\n");
				for (int i = 0; i < lvl; i++)
				{
					res.Append("\t");
				}
				res.Append("}\n");
				return (res.ToString());
			}
			public virtual void CheckTypes(ScopeDescriptor caller = null)
			{
				for (int i = 0; i < this.genericParameters.Count; i++)
				{
					if (this.genericParameters[i].extends != null)
					{
						this.genericParameters[i].extends.CheckType(caller);
					}
				}
				for (int i = 0; i < this.parameters.Count; i++)
				{
					this.parameters[i].type.CheckType(caller);
				}
				this.type.CheckType(caller);
			}
		}
		public class LambdaDescriptor : FunctionDescriptor
		{
			public LambdaDescriptor(string name, TypeDescriptor type, LanguageToken token, LanguageToken parent) : base(name, type, token, parent)
			{

			}
		}
		public class ParameterDescriptor : ValueDescriptor
		{
			public bool isOptionnal = false;
			public bool isInfinite = false;
			public string text = "";
			public LanguageToken parentMethod;
			public ParameterDescriptor(string name, TypeDescriptor type, LanguageToken parentMethod) : base(name, type)
			{
				this.parentMethod = parentMethod;
				;
			}
			public string ToString()
			{
				StringBuilder res = new StringBuilder();
				if (this.isInfinite)
				{
					res.Append("params ");
				}
				res.Append(this.type);
				if (this.isInfinite && this.type.typeName == "object")
				{
					res.Append("[]");
				}
				res.Append(" ");
				res.Append(this.name);
				if (this.isOptionnal && !isInfinite)
				{
					res.Append(" = default(" + this.type + ")");
				}
				return (res.ToString());
			}
		}

		public class MemberDescriptor : ValueDescriptor
		{
			public bool isPublic = true;
			public bool enumMember = false;
			public bool isStatic = false;
			public bool renamed = false;
			public LanguageToken token;
			public LanguageToken parent;
			public string formatedVarName = "";
			public MemberDescriptor(string name, TypeDescriptor type, LanguageToken token, LanguageToken parent, bool renamed = false, string formatedVarName = "") : base(name, type)
			{
				this.token = token;
				this.parent = parent;
				this.renamed = renamed;
				this.formatedVarName = formatedVarName;
				;
			}
			public virtual string ToString(int lvl = 0)
			{
				StringBuilder res = new StringBuilder();
				for (int i = 0; i < lvl; i++)
				{
					res.Append("\t");
				}
				if (this.renamed)
				{
					res.Append("[Name(\"" + this.name + "\")]");
					for (int i = 0; i < lvl; i++)
					{
						res.Append("\t");
					}
				}
				if (this.isPublic)
				{
					res.Append("public ");
				}
				if (this.isStatic)
				{
					res.Append("static ");
				}
				res.Append(this.type);
				res.Append(" ");
				if (!this.renamed)
					res.Append(this.name);
				else
					res.Append(this.formatedVarName);
				res.Append(";");
				res.Append("\n");
				return (res.ToString());
			}
		}
		public class MethodDescriptor : MemberDescriptor
		{
			public List<GenericTypeParameterDescriptor> genericParameters = new List<GenericTypeParameterDescriptor>();
			public List<ParameterDescriptor> parameters = new List<ParameterDescriptor>();
			public MethodDescriptor(string name, TypeDescriptor returnType, LanguageToken token, LanguageToken parent, bool renamed = false, string formatedVarName = "") : base(name, returnType, token, parent, renamed, formatedVarName)
			{
				;
			}
			public delegate R ActionGeneric<Q, R>(Q param);
			public object action;
			public virtual string ToString(int lvl = 0, bool isInterface = false)
			{
				List<GenericTypeParameterDescriptor> constrainteds = new List<GenericTypeParameterDescriptor>();
				StringBuilder res = new StringBuilder();
				for (int i = 0; i < lvl; i++)
				{
					res.Append("\t");
				}
				if (this.renamed)
				{
					res.Append("[Name(\"" + this.name + "\")]");
					for (int i = 0; i < lvl; i++)
					{
						res.Append("\t");
					}
				}
				if (this.isPublic && !isInterface)
					res.Append("public ");
				if (this.isStatic)
					res.Append("static ");
				res.Append(this.type);
				res.Append(" ");
				if (!this.renamed)
					res.Append(this.name);
				else
					res.Append(this.formatedVarName);
				if (this.genericParameters.Count > 0)
				{
					res.Append("<");
					for (int i = 0; i < this.genericParameters.Count; i++)
					{
						res.Append(this.genericParameters[i].name);
						if (this.genericParameters[i].extends != null)
						{
							constrainteds.Add(this.genericParameters[i]);
						}
						if (i + 1 < this.genericParameters.Count)
						{
							res.Append(", ");
						}
					}
					res.Append(">");
				}
				res.Append("(");
				for (int i = 0; i < this.parameters.Count; i++)
				{
					res.Append(this.parameters[i].ToString());
					if (i + 1 < this.parameters.Count)
					{
						res.Append(", ");
					}
				}
				res.Append(")");
				if (constrainteds.Count > 0)
				{

					for (int i = 0; i < constrainteds.Count; i++)
					{
						res.Append(" where ");
						res.Append(constrainteds[i].name);
						res.Append(" : ");
						res.Append(constrainteds[i].extends);
						if (i + 1 < constrainteds.Count)
							res.Append(" ");
					}
				}
				if (!isInterface)
				{
					res.Append("\n");

					for (int i = 0; i < lvl; i++)
					{
						res.Append("\t");
					}

					res.Append("{\n");
					for (int i = 0; i < lvl + 1; i++)
					{
						res.Append("\t");
					}

					res.Append("throw new System.NotImplementedException();\n");
					for (int i = 0; i < lvl; i++)
					{
						res.Append("\t");
					}
					res.Append("}\n");
				}
				else
				{
					res.Append(";\n");
				}

				return (res.ToString());
			}
			public interface Test
			{
				void smthg();
			}
			public virtual void CheckTypes(ScopeDescriptor caller = null)
			{
				for (int i = 0; i < this.genericParameters.Count; i++)
				{
					if (this.genericParameters[i].extends != null)
					{
						this.genericParameters[i].extends.CheckType(caller);
					}
				}
				for (int i = 0; i < this.parameters.Count; i++)
				{
					this.parameters[i].type.CheckType(caller);
				}
				if (this.type != null)
					this.type.CheckType(caller);
			}
		}
		public class ConstructorDescriptor : MethodDescriptor
		{
			public static ConstructorDescriptor defaultConstructor(ScopeDescriptor parentScope)
			{
				ConstructorDescriptor res = new ConstructorDescriptor(null, null, parentScope);
				res.isPublic = false;
				return (res);
			}
			public ScopeDescriptor parentScope = null;
			public ConstructorDescriptor(LanguageToken token, LanguageToken parent, ScopeDescriptor parentScope) : base("", null, token, parent)
			{
				this.parentScope = parentScope;
				;
			}
			public override string ToString(int lvl = 0, bool isInterface = false)
			{
				StringBuilder res = new StringBuilder();
				for (int i = 0; i < lvl; i++)
				{
					res.Append("\t");
				}
				if (this.isPublic)
				{
					res.Append("public ");
				}
				else
				{
					res.Append("protected ");
				}
				res.Append(this.parentScope.scopeName);
				res.Append("(");
				for (int i = 0; i < this.parameters.Count; i++)
				{
					res.Append(this.parameters[i].ToString());
					if (i + 1 < this.parameters.Count)
					{
						res.Append(", ");
					}
				}
				res.Append(")");
				if (parentScope.extends.Count > 0)
				{
					res.Append(" : base()");
				}
				res.Append("\n");
				for (int i = 0; i < lvl; i++)
				{
					res.Append("\t");
				}
				res.Append("{\n");
				for (int i = 0; i < lvl + 1; i++)
				{
					res.Append("\t");
				}

				res.Append("\n");
				for (int i = 0; i < lvl; i++)
				{
					res.Append("\t");
				}
				res.Append("}\n");

				return (res.ToString());
			}
		}
		public class IndexerDescriptorOld
		{
			public IndexerType type = IndexerType.String;
			public LanguageToken returnType = null;
			public IndexerDescriptorOld(IndexerType type, LanguageToken returnType)
			{
				//List < string[] > test 
				this.type = type;
				this.returnType = returnType;
				;
			}
		}


		public class IndexerDescriptor
		{
			public Action testouille;
			public TypeDescriptor indexerType = null;
			public TypeDescriptor returnType = null;
			public IndexerDescriptor(TypeDescriptor indexerType, TypeDescriptor returnType)
			{
				this.indexerType = indexerType;
				this.returnType = returnType;
			}
			public string Test<T, U>(T maurice, U smthg) where T : U
			{
				return ("");
			}
			public string ToString(int lvl)
			{
				StringBuilder res = new StringBuilder();
				//res.Append("")
				string indexerTXT = @"
public " + returnType + @" this[" + indexerType + @" index]
{
	get
	{
		return (" + returnType + @")((dynamic)this)[index];
	}				
	set
	{
		((dynamic)this)[index] = value;
	}
}
";

				string[] split = indexerTXT.Split('\n');
				for (int i = 0; i < split.Length; i++)
				{
					for (int j = 0; j < lvl; j++)
						res.Append("\t");
					res.Append(split[i]);
					res.Append("\n");
				}

				return (res.ToString());
			}
		}
		public class GenericTypeParameterDescriptor
		{
			public TypeDescriptor extends = null;
			public string name = "";
			public GenericTypeParameterDescriptor(string name, TypeDescriptor extends = null)
			{
				this.name = name;
				this.extends = extends;
			}
			public override string ToString()
			{
				StringBuilder res = new StringBuilder(this.name);

				return (res.ToString());
				;
			}
		}

		public class TypeDescriptor
		{
			public int dimension = 0;
			public static Dictionary<string, List<TypeDescriptor>> registeredTypes = new Dictionary<string, List<TypeDescriptor>>();
			public TypeDescriptor(string typeName, ScopeDescriptor calledFrom = null, int dimension = 0)
			{
				this.calledFrom = calledFrom;
				this.typeName = typeName;
				this.dimension = dimension;
				;
			}
			public string typeName;
			public ScopeDescriptor calledFrom = null; // son scope, le scope de tout les parents, les scopes definis dans les "import"
			public override string ToString()
			{
				StringBuilder array = new StringBuilder("");
				for (int i = 0; i < dimension; i++)
				{
					array.Append("[]");
				}
				return ("" + typeName + array.ToString());
				;
			}
			public virtual void CheckType(ScopeDescriptor caller = null)
			{
				if (caller == null)
					caller = this.calledFrom;
				;
			}
		}

		public class CompoundTypeDescriptor : TypeDescriptor
		{
			public new List<string> typeName = new List<string>();
			public CompoundTypeDescriptor(List<string> typeName, ScopeDescriptor calledFrom = null, int dimension = 0) : base("", null, dimension)
			{
				this.typeName = typeName;
				//throw new System.Exception("HEY?" + this.ToString());
			}
			public override string ToString()
			{
				StringBuilder res = new StringBuilder("");
				StringBuilder array = new StringBuilder("");
				for (int i = 0; i < dimension; i++)
				{
					array.Append("[]");
				}
				for (int i = 0; i < this.typeName.Count; i++)
				{
					res.Append(this.typeName[i]);
					if (i + 1 < this.typeName.Count)
					{
						res.Append(".");
					}
				}
				return (res.Append(array).ToString());
			}
			public bool CheckTypeHierarchy(int hierarchyIndex, ScopeDescriptor current, List<ScopeDescriptor> hierarchy, out List<ScopeDescriptor> trueHierarchy)
			{
				List<ScopeDescriptor> hierarchyClone = hierarchy.Clone();
				
				hierarchyClone.Add(current);
				trueHierarchy = hierarchyClone;
				if (hierarchyIndex >= this.typeName.Count)
				{
					
					return (true);
				}
				List<ScopeDescriptor> candidates = new List<ScopeDescriptor>();
				if (current.duplicates.ContainsKey(this.typeName[hierarchyIndex]))
				{
					for (int i = 0; i < current.duplicates[this.typeName[hierarchyIndex]].Count; i++)
					{
						if (current.scopeChildrens.ContainsKey(current.duplicates[this.typeName[hierarchyIndex]][i]))
						{
							candidates.Add(current.scopeChildrens[current.duplicates[this.typeName[hierarchyIndex]][i]]);
							;
						}
						else if (current.namespaceChildrens.ContainsKey(current.duplicates[this.typeName[hierarchyIndex]][i]))
						{
							candidates.Add(current.namespaceChildrens[current.duplicates[this.typeName[hierarchyIndex]][i]]);
							;
						}
					}
				}
				if (current.scopeChildrens.ContainsKey(this.typeName[hierarchyIndex]))
				{
					candidates.Add(current.scopeChildrens[this.typeName[hierarchyIndex]]);
				}
				if (candidates.Count > 0)
				{
					for (int i = 0; i < candidates.Count; i++)
					{
						if (CheckTypeHierarchy(hierarchyIndex + 1, candidates[i], hierarchyClone, out trueHierarchy))
						{
							return (true);
						}
					}
				}
				return (false);
				;
			}
			public override void CheckType(ScopeDescriptor caller = null)
			{
				if (caller == null)
					caller = this.calledFrom;
				if (caller != null)
				{
					List<ScopeDescriptor> hierarchy = null;
					ScopeDescriptor current = caller;
					while (current != null)
					{

						if (current.parentScope != null && CheckTypeHierarchy(0, current.parentScope, new List<ScopeDescriptor>(), out hierarchy))
						{
							break;
							;
						}
						else
							hierarchy = null;
						//if (current.scopeChildrens[this.typeName[0]])
						current = current.parentScope;
					}
					if (hierarchy != null)
					{
						List<string> newTypeName = new List<string>();
						StringBuilder hierarchyDebug = new StringBuilder("");
						for (int i = 0; i < hierarchy.Count; i++)
						{
							if (i > 0)
							{
								if (hierarchy[i].duplicate)
								{
									newTypeName.Add(hierarchy[i].duplicateName);
								}
								else
								{
									newTypeName.Add(hierarchy[i].scopeName);
								}
							}
						}
						this.typeName = newTypeName;
					}
				}
				//
			}
		}
		// pas utile de suite.
		public class LambdaFunctionDescriptor : TypeDescriptor
		{
			public bool isAction = false;
			public TypeDescriptor type = null;
			public List<ParameterDescriptor> parameters = new List<ParameterDescriptor>();
			public List<GenericTypeParameterDescriptor> genericParameters = new List<GenericTypeParameterDescriptor>();
			public LambdaFunctionDescriptor(string type, ScopeDescriptor calledFrom) : base(type, calledFrom)
			{

				;
			}
			public override string ToString()
			{
				StringBuilder res = new StringBuilder("");
				if (isAction)
					res.Append("Action");
				else
					res.Append("Func");
				if (this.parameters.Count > 0 || !isAction)
					res.Append("<");
				for (int i = 0; i < this.parameters.Count; i++)
				{
					res.Append(this.parameters[i].type.ToString());
					if (i + 1 < this.parameters.Count)
					{
						res.Append(", ");
					}


				}
				if (!isAction)
				{
					res.Append(", ");
					res.Append(this.type.ToString());
				}
				if (this.parameters.Count > 0 || !isAction)
					res.Append(">");
				return (res.ToString());

			}
			public override void CheckType(ScopeDescriptor caller = null)
			{
				if (caller == null)
					caller = this.calledFrom;
				foreach (ParameterDescriptor param in this.parameters)
				{
					param.type.CheckType(caller);
				}
				foreach (GenericTypeParameterDescriptor genericParam in this.genericParameters)
				{
					if (genericParam.extends != null)
					{
						genericParam.extends.CheckType(caller);
					}
				}
				if (this.type != null)
					this.type.CheckType(caller);
			}
		}
		public class GenericTypeDescriptor : TypeDescriptor
		{
			public TypeDescriptor genericType = null;
			public List<TypeDescriptor> parameters;
			public GenericTypeDescriptor(TypeDescriptor genericType, List<TypeDescriptor> parameters, ScopeDescriptor calledFrom) : base("", null)
			{
				this.parameters = parameters;
				this.genericType = genericType;

				;
			}
			public override string ToString()
			{
				StringBuilder res = new StringBuilder(this.genericType.ToString());
				res.Append("<");
				for (int i = 0; i < this.parameters.Count; i++)
				{
					res.Append(this.parameters[i].typeName);
					if (i + 1 < this.parameters.Count)
					{
						res.Append(", ");
					}
				}
				res.Append(">");
				return (res.ToString());
			}
			public override void CheckType(ScopeDescriptor caller = null)
			{
				if (caller == null)
					caller = this.calledFrom;
				foreach (TypeDescriptor type in this.parameters)
				{
					type.CheckType(caller);
				}
				this.genericType.CheckType(caller);
			}
		}
		public class AnyTypeDescriptor : TypeDescriptor
		{
			public List<TypeDescriptor> types;
			public AnyTypeDescriptor(List<TypeDescriptor> types, ScopeDescriptor calledFrom) : base("", null)
			{
				this.types = types;
				;
			}
			public override string ToString()
			{

				StringBuilder res = new StringBuilder("Any<");
				for (int i = 0; i < this.types.Count; i++)
				{
					res.Append(this.types[i]);
					if (i + 1 < this.types.Count)
					{
						res.Append(", ");
					}
				}
				res.Append(">");
				return (res.ToString());
			}
			public override void CheckType(ScopeDescriptor caller = null)
			{
				if (caller == null)
					caller = this.calledFrom;
				foreach (TypeDescriptor type in this.types)
				{
					type.CheckType(caller);
				}
			}
		}
		
		public class ScopeDescriptor
		{
			public bool isInterface = false;
			public bool isModule = false;
			public bool isObjectDescriptor = false;
			
			// on créé un délégué comme ça, et on s'en sert.
			//public delegate string str<T, U>(int marice, U smthg);
			public List<GenericTypeParameterDescriptor> genericParameters = new List<GenericTypeParameterDescriptor>();
			public string rename = "";
			public bool isNamespace = false;
			public bool isEnum = false;

			public string moduleName = "";

			public bool isStatic = false;
			public List<TypeDescriptor> references = new List<TypeDescriptor>();

			public List<TypeDescriptor> extends = new List<TypeDescriptor>();
			public List<TypeDescriptor> implements = new List<TypeDescriptor>();
			//public List<string> tmpExtends = new List<string>();
			//public List<string> tmpImplements = new List<string>();
			//public List<ScopeDescriptor> extends = new List<ScopeDescriptor>();
			//public List<ScopeDescriptor> implements = new List<ScopeDescriptor>();
			public ScopeDescriptor parentScope = null;
			public Dictionary<string, int> enumMembers = new Dictionary<string, int>();
			public List<IndexerDescriptor> indexers = new List<IndexerDescriptor>();
			public Dictionary<string, List<FunctionDescriptor>> functions = new Dictionary<string, List<FunctionDescriptor>>();
			public Dictionary<string, ValueDescriptor> values = new Dictionary<string, ValueDescriptor>();
			public Dictionary<string, ScopeDescriptor> namespaceChildrens = new Dictionary<string, ScopeDescriptor>();
			public Dictionary<string, ScopeDescriptor> scopeChildrens = new Dictionary<string, ScopeDescriptor>();
			public Dictionary<string, List<string>> duplicates = new Dictionary<string, List<string>>();
			public List<ConstructorDescriptor> constructors = new List<ConstructorDescriptor>();
			public Dictionary<string, MemberDescriptor> members = new Dictionary<string, MemberDescriptor>();
			public Dictionary<string, List<MethodDescriptor>> methods = new Dictionary<string, List<MethodDescriptor>>();
			public string scopeName;
			public string scopeType;
			public string duplicateName = "";
			public string test<I, J>(string p0, string p1)
			{
				;
				return ("");
			}

			public bool duplicate = false;
			public ScopeDescriptor(string structName, string structType, bool duplicate = false)
			{
				//  str<T, U>  dele = test<string, string>;

				this.scopeName = structName;
				this.scopeType = structType;
				this.duplicate = duplicate;

			}
			public void SetMember(MemberDescriptor descriptor)
			{
				if (this.isEnum)
				{
					this.enumMembers.Add(descriptor.name, this.enumMembers.Keys.Count);
				}
				else
					this.members[descriptor.name] = descriptor;
			}
			public void AddMethod(MethodDescriptor descriptor)
			{
				if (!(this.methods.ContainsKey(descriptor.name)))
				{
					this.methods.Add(descriptor.name, new List<MethodDescriptor>());
				}
				bool sameFound = false;
				for (int i = 0; i < this.methods[descriptor.name].Count; i++)
				{
					sameFound = true;
					if (descriptor.parameters.Count == this.methods[descriptor.name][i].parameters.Count && descriptor.genericParameters.Count == this.methods[descriptor.name][i].genericParameters.Count)
					{
						for (int j = 0; j < this.methods[descriptor.name][i].parameters.Count; j++)
						{
							if (descriptor.parameters[j].type.ToString() != this.methods[descriptor.name][i].parameters[j].type.ToString())
							{
								sameFound = false;
								break;
							}
						}
					}
					else
					{
						sameFound = false;
					}
					if (sameFound)
					{
						break;
					}
				}
				if (!sameFound)
					this.methods[descriptor.name].Add(descriptor);
				// check si on a une methode avec les memes parametres eventuellement.
			}
			public void AddConstructor(ConstructorDescriptor constructor)
			{
				this.constructors.Add(constructor);
			}
			public void SetValue(ValueDescriptor value)
			{
				this.values[value.name] = value;
			}
			public void AddFunction(FunctionDescriptor descriptor)
			{
				if (!(this.functions.ContainsKey(descriptor.name)))
				{
					this.functions.Add(descriptor.name, new List<FunctionDescriptor>());
				}
				this.functions[descriptor.name].Add(descriptor);
			}
			public void WorkDuplicates()
			{
				if (this.members.Count > 0 || this.values.Count > 0 || this.functions.Count > 0 || this.methods.Count > 0) // DUPLICAT
				{
					this.isNamespace = false;
				}
				if (!this.isNamespace)
				{

					List<string> toRemove = new List<string>();
					foreach (string key in this.namespaceChildrens.Keys)
					{
						toRemove.Add(key);
					}
					for (int i = 0; i < toRemove.Count; i++)
					{
						ScopeDescriptor scope = this.namespaceChildrens[toRemove[i]];
						if (scope.members.Count > 0 || this.values.Count > 0 || this.functions.Count > 0 || this.methods.Count > 0)
							scope.isNamespace = false;
						if (!scope.isNamespace)
						{
							string varName = toRemove[i];
							this.namespaceChildrens.Remove(toRemove[i]);
							if (this.scopeChildrens.ContainsKey(toRemove[i]))
							{
								string trueVarName = varName;
								for (int j = 0; j >= 0 && this.scopeChildrens.ContainsKey(trueVarName); j++)
								{
									trueVarName = varName + "_";
									if (j > 0)
									{
										trueVarName = varName + "_" + j;
									}
									Console.WriteLine("name:" + trueVarName);
								}

								scope.isNamespace = false;
								scope.duplicate = true;
								scope.duplicateName = trueVarName;
							}
							// HERE
							if (!this.duplicates.ContainsKey(scope.scopeName))
							{
								this.duplicates[scope.scopeName] = new List<string>();
							}
							this.duplicates[scope.scopeName].Add(scope.duplicateName);
							if (this.scopeChildrens.ContainsKey(scope.duplicateName))
							{
								Console.WriteLine("NAME:" + scope.duplicateName);
							}
							if (scope.duplicate)
								this.scopeChildrens.Add(scope.duplicateName, scope);
							else
								this.scopeChildrens.Add(scope.scopeName, scope);
						}
					}
				}
				foreach (string key in this.namespaceChildrens.Keys)
				{
					this.namespaceChildrens[key].WorkDuplicates();
				}
				foreach (string key in this.scopeChildrens.Keys)
				{
					this.scopeChildrens[key].WorkDuplicates();
				}

			}

			public void CheckTypes()
			{
				// chaque membre chaque methode, chaque constructeur, chaque tout vient vérif si il parle pas d'un duplicate si il a un compoundType.
				foreach (MemberDescriptor member in this.members.Values)
				{
					member.type.CheckType(this);
				}
				foreach (List<MethodDescriptor> methods in this.methods.Values)
				{
					foreach (MethodDescriptor method in methods)
					{
						method.CheckTypes(this);
					}
				}
				foreach (ConstructorDescriptor constructor in this.constructors)
				{
					constructor.CheckTypes(this);
				}
				foreach (GenericTypeParameterDescriptor genericParam in this.genericParameters)
				{
					if (genericParam.extends != null)
					{
						genericParam.extends.CheckType();
					}
				}
				foreach (List<FunctionDescriptor> functions in this.functions.Values)
				{
					foreach (FunctionDescriptor function in functions)
					{
						function.CheckTypes(this);
					}
				}
				foreach (TypeDescriptor type in this.extends)
				{
					type.CheckType(this);
				}
				foreach (TypeDescriptor type in this.implements)
				{
					type.CheckType(this);
				}
				foreach (IndexerDescriptor indexer in this.indexers)
				{
					indexer.returnType.CheckType(this);
				}
				foreach (ValueDescriptor value in this.values.Values)
				{
					value.type.CheckType(this);
				}
				foreach (ScopeDescriptor scope in this.scopeChildrens.Values)
				{
					scope.CheckTypes();
				}
				foreach (ScopeDescriptor scope in this.namespaceChildrens.Values)
				{
					scope.CheckTypes();
				}
				;
			}

			public string GetTrims(int lvl)
			{
				StringBuilder res = new StringBuilder();
				for (int i = 0; i < lvl; i++)
				{
					res.Append("\t");
				}
				return (res.ToString());
			}
			public string ToString(int lvl = 0)
			{

				if (this.members.Count > 0)
				{

					List<string> toRemove = new List<string>();
					int count = 0;
					foreach (string key in this.members.Keys)
					{

						MemberDescriptor desc = this.members[key];
						TypeDescriptor type = this.members[key].type;
						if (this.scopeChildrens.ContainsKey(type.typeName)) //!! attention anys?
						{
							ScopeDescriptor child = this.scopeChildrens[type.typeName];
							if (child.isEnum)
							{
								child.SetMember(desc);
								toRemove.Add(desc.name);
							}
						}
					}
					for (int i = 0; i < toRemove.Count; i++)
					{
						this.members.Remove(toRemove[i]);
					}
				}
				bool check = false;
				for (int i = 0; i < this.constructors.Count; i++)
				{
					if (this.constructors[i].parameters.Count == 0)
					{
						check = true;
						break;
					}
				}
				if (!check && !isObjectDescriptor)
				{
					this.constructors.Add(ConstructorDescriptor.defaultConstructor(this));
				}
				StringBuilder res = new StringBuilder();
				res.Append("\n");
				res.Append(GetTrims(lvl));
				bool trueDuplicate = false;
				switch (this.scopeType)
				{
					case "interfaceKW":
						/*if (this.members.Count == 0)
						{
							res.Append("public interface ");
							this.isInterface = true;
							;
						}
						else
						{*/
						if (this.methods.Count == 0)
							res.Append("[ObjectLiteral]\n");
						else
							res.Append("[ExternalAttribute]\n");
						res.Append(GetTrims(lvl));
						res.Append("public class ");
						//}
						break;
					case "classKW":
						if (this.methods.Count == 0)
							res.Append("[ObjectLiteral]\n");
						else
							res.Append("[ExternalAttribute]\n");
						res.Append(GetTrims(lvl));
						res.Append("public class ");
						break;
					case "namespaceKW":
						if (this.members.Count > 0 || this.values.Count > 0 || this.functions.Count > 0 || this.methods.Count > 0 || !this.isNamespace)
						{
							if (this.duplicate)
							{
								res.Append("[Name(\"" + this.scopeName + "\")]\n");
								res.Append(GetTrims(lvl));
								trueDuplicate = true;
							}
							res.Append("[ExternalAttribute]\n");
							res.Append(GetTrims(lvl));
							res.Append("public static class ");
							this.isStatic = true;
						}
						else
						{
							//throw new System.Exception("???");
							res.Append("namespace ");
						}
						break;
					case "enumKW":
						res.Append("[Enum(Emit.StringNameLowerCase)]\n");
						res.Append(GetTrims(lvl));
						res.Append("public enum ");
						break;
					case "moduleKW":
						res.Append("[Module(\"" + moduleName + "\")]\n");
						res.Append(GetTrims(lvl));
						res.Append("public class ");
						break;
				}

				if (this.duplicate)
					res.Append(this.duplicateName);
				else
					res.Append(this.scopeName);
				List<GenericTypeParameterDescriptor> constrainteds = new List<GenericTypeParameterDescriptor>();
				if (this.genericParameters.Count > 0)
				{
					res.Append("<");
					for (int i = 0; i < this.genericParameters.Count; i++)
					{
						res.Append(this.genericParameters[i].ToString());
						if (this.genericParameters[i].extends != null)
						{
							constrainteds.Add(this.genericParameters[i]);
						}
						if (i + 1 < this.genericParameters.Count)
							res.Append(", ");
					}
					res.Append(">");
				}
				if (this.extends.Count > 0 || this.implements.Count > 0)
					res.Append(" : ");
				int k = 0;


				for (int i = 0; i < this.extends.Count; i++)
				{
					res.Append(this.extends[i].ToString());
					if (i + 1 < this.extends.Count)
						res.Append(", ");
				}
				if (this.extends.Count > 0 && this.implements.Count > 0)
				{
					res.Append(", ");
				}
				for (int i = 0; i < this.implements.Count; i++)
				{
					res.Append(this.implements[i].ToString());
					if (i + 1 < this.implements.Count)
						res.Append(", ");

				}
				if (constrainteds.Count > 0)
				{

					for (int i = 0; i < constrainteds.Count; i++)
					{
						res.Append(" where ");
						res.Append(constrainteds[i].name);
						res.Append(" : ");
						res.Append(constrainteds[i].extends);
						if (i + 1 < constrainteds.Count)
							res.Append(" ");
					}
				}
				res.Append("\n");
				res.Append(GetTrims(lvl));
				res.Append("{\n");
				foreach (string key in this.enumMembers.Keys)
				{
					res.Append(GetTrims(lvl + 1));
					res.Append(key);
					if (k + 1 < this.enumMembers.Keys.Count)
						res.Append(",\n");
					else
						res.Append("\n");
					k++;
				}
				if (!this.isEnum && !this.isNamespace && !this.isStatic && !this.isInterface)
				{
					foreach (ConstructorDescriptor constructor in this.constructors)
					{
						res.Append(constructor.ToString(lvl + 1));
					}
				}
				for (int i = 0; i < this.indexers.Count; i++)
				{
					res.Append(this.indexers[i].ToString(lvl + 1));
				}
				foreach (string key in this.members.Keys)
				{
					if (this.isStatic)
						this.members[key].isStatic = true;
					res.Append(this.members[key].ToString(lvl + 1));
				}
				foreach (string key in this.methods.Keys)
				{
					List<MethodDescriptor> methods = this.methods[key];
					for (int i = 0; i < methods.Count; i++)
					{
						if (this.isStatic)
							methods[i].isStatic = true;
						res.Append(methods[i].ToString(lvl + 1, this.isInterface));
					}
				}
				foreach (string key in this.functions.Keys)
				{
					List<FunctionDescriptor> functions = this.functions[key];
					for (int i = 0; i < functions.Count; i++)
					{
						res.Append(functions[i].ToString(lvl + 1));
					}
				}
				foreach (string key in this.scopeChildrens.Keys)
				{
					res.Append(this.scopeChildrens[key].ToString(lvl + 1));
				}
				foreach (string key in this.namespaceChildrens.Keys)
				{
					res.Append(this.namespaceChildrens[key].ToString(lvl + 1));
				}
				res.Append(GetTrims(lvl));
				res.Append("}");
				res.Append("\n");
				return (res.ToString());
			}
		}
		/*public class Test<T, U>
		{
			public Test()
			{
				;
			}
		}
		public class Test2<T, U> : Test<T, U> where T : Test<T, U>
		{
			public Test2(string smthg) : base()
			{
			
			}
		}*/
	}

}
